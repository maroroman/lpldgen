#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "generated/payloads.h"
#include "generated/pld_testpld.h"
#include "../src/test_framework.h"

static bool test_simple(void) {
    pld_error_t ret = 0;

    /* Source data */
    struct TestPld_test1 pld = {
        .foo = 0xF0,
    };

    const size_t sz = TESTPLD_TEST1_BIN_SIZE;

    uint8_t packed[sz];

    /* Pack */
    ret = TestPld_test1_build_s(&pld, packed, sz);
    check(ret == PLD_OK);

    /* Check packed format */
    const uint8_t expected[] = { 0xF0 };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test1 pld2;
    ret = TestPld_test1_parse_s(packed, sz, &pld2);
    check(ret == PLD_OK);

    /* Compare with original */
    check_eq(pld2.foo, pld.foo);

    return true;
}

static struct Test tests[] = {
    {"test_simple", test_simple},
    {}
};

int main() {
    printf("main_noalloc() runs...\n\n");

    run_tests(tests, "minimal");
    return 0;
}
