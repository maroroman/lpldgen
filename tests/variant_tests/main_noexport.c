#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "generated/payloads.h"
#include "generated/pld_testpld.h"
#include "../src/test_framework.h"

bool test1(void) {
    uint8_t *packed = NULL;
    size_t len = 0;
    pld_error_t ret = 0;

    /* Source data */
    struct TestPld_test1 pld = {
        .foo = 0xF0,
    };

    /* Pack */
    ret = TestPld_test1_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 1ul);

    /* Check packed format */
    const uint8_t expected[] = { 0xF0 };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test1 *pld2 = NULL;
    ret = TestPld_test1_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->foo, pld.foo);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}

bool test1_generic_build(void) {
    uint8_t *packed = NULL;
    size_t len = 0;
    pld_error_t ret = 0;

    const pld_module_t *modules[] = PLD_MODULES(&Payloads_TestPld);

    /* Source data */
    struct TestPld_test1 pld = {
        .pld_id = TESTPLD_TEST1_ID,
        .foo = 0xF0,
    };

    /* Pack */
    ret = pld_build(modules, &pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 1ul);

    /* Check packed format */
    const uint8_t expected[] = { 0xF0 };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test1 *pld2 = NULL;
    ret = pld_parse_as(modules, TESTPLD_TEST1_ID, packed, len, malloc, (void**) &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);
    check_eq(pld2->pld_id, TESTPLD_TEST1_ID);

    /* Compare with original */
    check_eq(pld2->foo, pld.foo);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}

static struct Test tests[] = {
    {"test1", test1},
    {"test1_generic_build", test1_generic_build},
    {}
};

int main() {
    printf("main_noexport() runs...\n\n");

    run_tests(tests, "noexport");
    return 0;
}
