/**
 * TESTS
 * 
 * Created on 2020/01/11.
 */

#ifndef LIBPAYLOAD_TEST_H
#define LIBPAYLOAD_TEST_H

#include "pld_testpld.h"
#include "pld_testpld2.h"
#include "pld_exp.h"

#include "test_framework.h"

// add all tests to this macro in the format X(func_name)
#define XTESTS \
    X(test_asserts) \
    X(test_u8) \
    X(test_u8_generic_build) \
    X(test_u8_static) \
    X(test_i8) \
    X(test_u16) \
    X(test_i16) \
    X(test_u24) \
    X(test_i24) \
    X(test_u32) \
    X(test_i32) \
    X(test_u64) \
    X(test_i64) \
    X(test_f32) \
    X(test_f64) \
    X(test_bool) \
    X(test_pld_endian_little) \
    X(test_pld_endian_big) \
    X(test_simple_struct) \
    X(test_two_structs) \
    X(test_nested_struct) \
    X(test_primitive_array) \
    X(test_structs_array) \
    X(test_simple_bitfield) \
    X(test_bitfield_in_struct) \
    X(test_bitfield_nested) \
    X(test_bitfield_arrays) \
    X(test_convert) \
    X(test_tail_primitive) \
    X(test_tail_struct) \
    X(test_tail_bits) \
    X(test_base_recog) \
    X(test_recog_const_first) \
    X(test_recog_const_last) \
    X(test_recog_const_stru) \
    X(test_recog_const_multiple) \
    X(test_recog_const_bf_single) \
    X(test_recog_const_bf_multiple) \
    X(test_recog_const_padded) \
    X(test_recog_const_string) \
    X(test_export_smoke)

#endif //LIBPAYLOAD_TEST_H
