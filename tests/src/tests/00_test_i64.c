#include "../test.h"

bool test_i64(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_i64 pld = {
        .i64b = -7768127720449730441ll,
        .i64l = -7768127720449730441ll,
        .i64b2 = 0x6BCDEF0123456789ull,
        .i64l2 = 0x6BCDEF0123456789ull,
    };
    check_eq_l(pld.i64b, -7768127720449730441l);

    /* Pack */
    ret = TestPld_test_i64_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 32ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0x94, 0x32, 0x10, 0xFE, 0xDC, 0xBA, 0x98, 0x77,
        // little
        0x77, 0x98, 0xBA, 0xDC, 0xFE, 0x10, 0x32, 0x94,
        // big
        0x6B, 0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67, 0x89,
        // little
        0x89, 0x67, 0x45, 0x23, 0x01, 0xEF, 0xCD, 0x6B
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_i64 *pld2 = NULL;
    ret = TestPld_test_i64_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq_l(pld2->i64b, pld.i64b);
    check_eq_l(pld2->i64l, pld.i64l);
    check_eq_l(pld2->i64b2, pld.i64b2);
    check_eq_l(pld2->i64l2, pld.i64l2);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
