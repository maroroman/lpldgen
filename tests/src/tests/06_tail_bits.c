#include "../test.h"

bool test_tail_bits(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_tail_bits *pld = malloc(TESTPLD_TAIL_BITS_C_SIZE(3));
    pld->pld_id = TESTPLD_TAIL_BITS_ID; // malloc can have unallocated bytes: use calloc, or set ID!
    pld->tail_len = 3;
    pld->tail[0].a = 0x10;
    pld->tail[0].b = 0x7;
    pld->tail[1].a = 0x1E;
    pld->tail[1].b = 0x6;
    pld->tail[2].a = 0x01;
    pld->tail[2].b = 0x2;

    /* Pack */
    ret = TestPld_tail_bits_build(pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 3ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0x87, 0xF6, 0x0A
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_tail_bits *pld2 = NULL;
    ret = TestPld_tail_bits_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq_ul(pld2->tail_len, 3ul);
    check_eq(pld->tail[0].a, pld2->tail[0].a);
    check_eq(pld->tail[0].b, pld2->tail[0].b);
    check_eq(pld->tail[1].a, pld2->tail[1].a);
    check_eq(pld->tail[1].b, pld2->tail[1].b);
    check_eq(pld->tail[2].a, pld2->tail[2].a);
    check_eq(pld->tail[2].b, pld2->tail[2].b);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
