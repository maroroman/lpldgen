#include "../test.h"

bool test_primitive_array(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_primitive_array pld = {
        //bool b[2];
        .b = {true, false},//2
        //uint8_t u8[2];
        .u8 = {1,2},//4
        //uint16_t u16[2];
        .u16 = {0xABCD, 0xEF01},//8
        //uint32_t u24[2];
        .u24 = {0xABCDEF, 0x123456},//14
        //uint32_t u32[2];
        .u32 = {0x12345678, 0xABCD1234},//22
        //float f[2];
        .f = {1.1f, 0.2f},//30
        //int8_t i8[2];
        .i8 = {127, -128},//32
        //int16_t i16[2];
        .i16 = {32767, -32768},//36
        //int32_t i24[2];
        .i24 = {1234, -1234},//42
        //int32_t i32[2];
        .i32 = {123456, -123456},//50
        //double d[2];
        .d = {3.15, 768.123},//66
        // uint8_t small[1];
        .small = {15},//67
    };

    /* Pack */
    ret = TestPld_primitive_array_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 67ul);

    /* Check packed format */
    const uint8_t expected[] = {
        1, /**/ 0,
        1, /**/ 2,
        0xab, 0xcd, /**/ 0xef, 0x01,
        0xab, 0xcd, 0xef, /**/ 0x12, 0x34, 0x56,
        0x12, 0x34, 0x56, 0x78, /**/ 0xab, 0xcd, 0x12, 0x34,
        0x3f, 0x8c, 0xcc, 0xcd, /**/ 0x3e, 0x4c, 0xcc, 0xcd, // floats
        0x7F, /**/ 0x80,
        0x7F, 0xFF, /**/ 0x80, 0x00,
        0x00, 0x04, 0xD2, /**/ 0xFF, 0xFB, 0x2E,
        0x00, 0x01, 0xE2, 0x40, /**/ 0xFF, 0xFE, 0x1D, 0xC0,
        0x40, 0x09, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, /**/ 0x40, 0x88, 0x00, 0xfb, 0xe7, 0x6c, 0x8b, 0x44, // doubles
        15
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_primitive_array *pld2 = NULL;
    ret = TestPld_primitive_array_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->b[0], pld.b[0]);
    check_eq(pld2->b[1], pld.b[1]);

    check_eq(pld2->u8[0], pld.u8[0]);
    check_eq(pld2->u8[1], pld.u8[1]);

    check_eq(pld2->u16[0], pld.u16[0]);
    check_eq(pld2->u16[1], pld.u16[1]);

    check_eq(pld2->u24[0], pld.u24[0]);
    check_eq(pld2->u24[1], pld.u24[1]);

    check_eq(pld2->u32[0], pld.u32[0]);
    check_eq(pld2->u32[1], pld.u32[1]);

    check_eq_f(pld2->f[0], pld.f[0]);
    check_eq_f(pld2->f[1], pld.f[1]);

    check_eq(pld2->i8[0], pld.i8[0]);
    check_eq(pld2->i8[1], pld.i8[1]);

    check_eq(pld2->i16[0], pld.i16[0]);
    check_eq(pld2->i16[1], pld.i16[1]);

    check_eq(pld2->i24[0], pld.i24[0]);
    check_eq(pld2->i24[1], pld.i24[1]);

    check_eq_f(pld2->d[0], pld.d[0]);
    check_eq_f(pld2->d[1], pld.d[1]);

    check_eq(pld2->small[0], pld.small[0]);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
