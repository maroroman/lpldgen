#include "../test.h"

bool test_nested_struct(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_nested_struct pld = {
        .x = {
            .a = 0xFE,
            .y = {
                .a = 0xED,
                .y = {
                    .a = 0xCA,
                    .b = 0xFE
                },
                .c = 0xBE,
            },
            .c = 0xA5,
        },
        .c = 0x75
    };

    /* Pack */
    ret = TestPld_nested_struct_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 7ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0xfe, 0xed, 0xca, 0xfe, 0xbe, 0xa5, 0x75
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_nested_struct *pld2 = NULL;
    ret = TestPld_nested_struct_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->x.a, pld.x.a);
    check_eq(pld2->x.y.a, pld.x.y.a);
    check_eq(pld2->x.y.y.a, pld.x.y.y.a);
    check_eq(pld2->x.y.y.b, pld.x.y.y.b);
    check_eq(pld2->x.y.c, pld.x.y.c);
    check_eq(pld2->x.c, pld.x.c);
    check_eq(pld2->c, pld.c);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
