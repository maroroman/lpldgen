#include "../test.h"

bool test_bitfield_in_struct(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_bitf_inside pld = {
        .wrap = {
            .bits = {
                .upper = 0x5,
                .lower = 0x19,
            },
            .field = 0xCD,
        },
        .array = {
            { .upper = 0x5, .lower = 0x19 },
            { .upper = 0x6, .lower = 0x1A },
        },
    };

    /* Pack */
    ret = TestPld_bitf_inside_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 4ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0xB9, 0xCD,
        0xB9, 0xDA,
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_bitf_inside *pld2 = NULL;
    ret = TestPld_bitf_inside_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->wrap.bits.upper, pld.wrap.bits.upper);
    check_eq(pld2->wrap.bits.lower, pld.wrap.bits.lower);
    check_eq(pld2->wrap.field, pld.wrap.field);
    check_eq(pld2->array[0].upper, pld.array[0].upper);
    check_eq(pld2->array[0].lower, pld.array[0].lower);
    check_eq(pld2->array[1].upper, pld.array[1].upper);
    check_eq(pld2->array[1].lower, pld.array[1].lower);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
