#include "../test.h"

bool test_base_recog(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;
    void *vpld = NULL;
    uint32_t kind;

    const pld_module_t *modules[] = PLD_MODULES(&Payloads_TestPld);

    /* r_two */
    {
        /* Source data */
        struct TestPld_r_two pld = {
            .buf = {1, 2},
        };

        /* Pack */
        ret = TestPld_r_two_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        /* Parse as generic payload */
        ret = pld_parse(modules, (struct pld_address) {7, 0, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);

        /* Compare with original */
        check_eq(kind, TESTPLD_R_TWO_ID);
        struct TestPld_r_two *pld2 = vpld;

        check_eq(pld2->buf[0], pld.buf[0]);
        check_eq(pld2->buf[1], pld.buf[1]);

        /* Clean up */
        free(pld2);
        free(packed);
    }

    /* r_three */
    {
        /* Source data */
        struct TestPld_r_three pld = {
            .buf = {1, 2, 3},
        };

        /* Pack */
        ret = TestPld_r_three_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        /* Parse as generic payload */
        ret = pld_parse(modules, (struct pld_address) {7, 0, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);

        /* Compare with original */
        check_eq(kind, TESTPLD_R_THREE_ID);
        struct TestPld_r_three *pld2 = vpld;

        check_eq(pld2->buf[0], pld.buf[0]);
        check_eq(pld2->buf[1], pld.buf[1]);
        check_eq(pld2->buf[2], pld.buf[2]);

        /* Clean up */
        free(pld2);
        free(packed);
    }

    /* r_two_to, r_two_b */
    {
        /* Source data */
        struct TestPld_r_two_to pld = {
            .buf = {1, 2},
        };

        /* Pack */
        ret = TestPld_r_two_to_build(&pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        /* Parse as generic payload */
        ret = pld_parse(modules, (struct pld_address) {30, 30, 7, 0}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);

        /* Compare with original */
        check_eq(kind, TESTPLD_R_TWO_TO_ID);
        struct TestPld_r_two_to *pld2 = vpld;

        check_eq(pld2->buf[0], pld.buf[0]);
        check_eq(pld2->buf[1], pld.buf[1]);

        /* Clean up */
        free(vpld);

        /**
         * Now try to parse it as r_two (from instead of to)
         * - it will work, since the payloads are formatted the same
         */
        ret = pld_parse(modules, (struct pld_address) {7, 0, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD_R_TWO_ID);
        free(vpld);

        /**
         * If both addresses/ports match, the first defined payload wins
         */
        ret = pld_parse(modules, (struct pld_address) {7, 0, 7, 0}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD_R_TWO_ID); // was defined earlier in the spec file
        free(vpld);

        /**
         * Parse as r_two_b - port 1
         */
        ret = pld_parse(modules, (struct pld_address) {7, 1, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);
        check_eq(kind, TESTPLD_R_TWO_B_ID);
        struct TestPld_r_two_b *pld3 = vpld;
        check_eq(pld3->buf[0], pld.buf[0]);
        check_eq(pld3->buf[1], pld.buf[1]);
        free(vpld);

        free(packed);
    }

    /* r_tail */
    {
        /* Source data */
        struct TestPld_r_tail *pld = calloc(TESTPLD_R_TAIL_C_SIZE(0), 1);
        // if calloc is used, ID is 0 and not checked.
        pld->tail_len = 0;
        pld->buf[0] = 1;
        pld->buf[1] = 2;
        pld->buf[2] = 3;

        /* Pack */
        ret = TestPld_r_tail_build(pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        /* Parse as generic payload */
        ret = pld_parse(modules, (struct pld_address) {7, 1, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);

        /* Compare with original */
        check_eq(kind, TESTPLD_R_TAIL_ID);
        struct TestPld_r_tail *pld2 = vpld;

        check_eq(pld2->buf[0], pld->buf[0]);
        check_eq(pld2->buf[1], pld->buf[1]);
        check_eq(pld2->buf[2], pld->buf[2]);

        /* Clean up */
        free(pld2);
        free(packed);
    }

    /* r_tail with longer tail */
    {
        /* Source data */
        struct TestPld_r_tail *pld = malloc(TESTPLD_R_TAIL_C_SIZE(2));
        pld->pld_id = 0; // this disables the check in _build()
        pld->tail_len = 2;
        pld->buf[0] = 1;
        pld->buf[1] = 2;
        pld->buf[2] = 3;
        pld->tail[0] = 123;
        pld->tail[1] = 456;

        /* Pack */
        ret = TestPld_r_tail_build(pld, malloc, &packed, &len);
        check_eq(ret, PLD_OK);

        /* Parse as generic payload */
        ret = pld_parse(modules, (struct pld_address) {7, 1, 30, 30}, packed, len, malloc, &vpld, &kind);
        check_eq(ret, PLD_OK);

        // test that addr can be retrieved by payload ID
        struct pld_address addr;
        ret = pld_get_addr_by_id(modules, TESTPLD_R_TAIL_ID, &addr);
        check_eq(ret, PLD_OK);
        check_eq(addr.src, 7);
        check_eq(addr.sport, 1);
        check_eq(addr.dst, 255);
        check_eq(addr.dport, 255);

        // test that addr can be retrieved by payload instance too
        addr.src = 0;
        addr.sport = 0;
        addr.dst = 0;
        addr.dport = 0;
        ret = pld_get_addr(modules, vpld, &addr);
        check_eq(ret, PLD_OK);
        check_eq(addr.src, 7);
        check_eq(addr.sport, 1);
        check_eq(addr.dst, 255);
        check_eq(addr.dport, 255);

        /* Compare with original */
        check_eq(kind, TESTPLD_R_TAIL_ID);
        struct TestPld_r_tail *pld2 = vpld;

        check_eq(pld2->buf[0], pld->buf[0]);
        check_eq(pld2->buf[1], pld->buf[1]);
        check_eq(pld2->buf[2], pld->buf[2]);
        check_eq_ul(pld2->tail_len, pld->tail_len);
        check_eq(pld2->tail[0], pld->tail[0]);
        check_eq(pld2->tail[1], pld->tail[1]);

        /* Clean up */
        free(pld2);
        free(packed);
    }

    return true;
}
