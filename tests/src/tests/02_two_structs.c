#include "../test.h"

bool test_two_structs(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_two_structs pld = {
        .x = {
            .a = 0xAB,
            .b = 0x01,
        },
        .y = {
            .a = 0xCDEF,
            .b = -16,
        },
    };

    /* Pack */
    ret = TestPld_two_structs_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 5ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0xab, 0x01, 0xEF, 0xCD, 0xF0
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_two_structs *pld2 = NULL;
    ret = TestPld_two_structs_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->x.a, pld.x.a);
    check_eq(pld2->x.b, pld.x.b);
    check_eq(pld2->y.a, pld.y.a);
    check_eq(pld2->y.b, pld.y.b);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
