#include "../test.h"

bool test_u24(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_u24 pld = {
        .u24b = 0xABCDEF,
        .u24l = 0xABCDEF,
    };

    /* Pack */
    ret = TestPld_test_u24_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 6ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0xAB, 0xCD, 0xEF,
        // little
        0xEF, 0xCD, 0xAB
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_u24 *pld2 = NULL;
    ret = TestPld_test_u24_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->u24b, pld.u24b);
    check_eq(pld2->u24l, pld.u24l);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
