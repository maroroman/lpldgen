#include "../test.h"

bool test_convert(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_convert pld = {
        .f_u8 = 255.0f,
        .f_u16 = 65534.0f,
        .f_u32 = 12345678.0f,
        .f_u64 = 12345678.0f,
        .f_i8 = -128.0f,
        .f_i16 = -32768.0f,
        .f_i32 = -12346578.0f,
        .f_i64 = -12346578.0f,
        .d_f = 123465.789062, // double stored as float
        .f_d = 123465.789062f, // float stored as double
        .in_struct = {
            .degrees = 27.15f,
            .voltage = 4575, // u16
            .plus_5 = 31,
            .div10_f = 123.0f,
            .wide_bool = true,
        }
    };

    /* Pack */
    ret = TestPld_convert_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 52ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // f_u8
        255,
        // f_u16
        0xFF, 0xFE,
        // f_u32
        0x00, 0xbc, 0x61, 0x4e,
        // f_u64
        0x00, 0x00, 0x00, 0x00, 0x00, 0xbc, 0x61, 0x4e,
        // f_i8
        0x80,
        // f_i16
        0x80, 0x00,
        // f_i32
        0xff, 0x43, 0x9b, 0x2e,
        // f_i64
        0xff, 0xff, 0xff, 0xff, 0xff, 0x43, 0x9b, 0x2e,
        // d_f
        0x47, 0xF1, 0x24, 0xe5,
        // f_d
        0x40, 0xfe, 0x24, 0x9c, 0xa0, 0x00, 0x00, 0x00,
        // degrees
        0x12, 0x6b,
        // voltage
        183,
        // plus_5
        36,
        // div10_f
        0x41, 0x44, 0xcc, 0xcd,
        // wide_bool
        0x00, 0x01
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_convert *pld2 = NULL;
    ret = TestPld_convert_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq_f(pld2->f_u8, pld.f_u8);
    check_eq_f(pld2->f_u16, pld.f_u16);
    check_eq_f(pld2->f_u32, pld.f_u32);
    check_eq_f(pld2->f_u64, pld.f_u64);
    check_eq_f(pld2->f_i8, pld.f_i8);
    check_eq_f(pld2->f_i16, pld.f_i16);
    check_eq_f(pld2->f_i32, pld.f_i32);
    check_eq_f(pld2->f_i64, pld.f_i64);
    check_eq_f_safe(pld2->d_f, pld.d_f, 0.001);
    check_eq_f_safe(pld2->f_d, pld.f_d, 0.001);

    check_eq_f_safe(pld2->in_struct.degrees, pld.in_struct.degrees, 0.001);
    check_eq(pld2->in_struct.voltage, pld.in_struct.voltage);
    check_eq(pld2->in_struct.plus_5, pld.in_struct.plus_5);
    check_eq_f(pld2->in_struct.div10_f, pld.in_struct.div10_f);
    check_eq(pld2->in_struct.wide_bool, pld.in_struct.wide_bool);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
