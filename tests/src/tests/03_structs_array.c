#include "../test.h"

bool test_structs_array(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_structs_array pld = {
        .nested = {
            // 0
            {
                .a = 0xA0,
                .b = 0xB0B1,
                .inner = {
                    // 0
                    {
                        .a = 0xA0,
                        .c = 0xC0
                    },
                    {
                        .a = 0xA1,
                        .c = 0xC1
                    },
                },
                .z = 0x78
            },
            // 1
            {
                .a = 0xA5,
                .b = 0xB5B6,
                .inner = {
                    // 0
                    {
                        .a = 0xA5,
                        .c = 0xC5
                    },
                    {
                        .a = 0xA6,
                        .c = 0xC6
                    },
                },
                .z = 0x89
            }
        },
        .single = {
            // 0
            {
                .f = 0xF0
            },
        },
    };

    /* Pack */
    ret = TestPld_structs_array_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 17ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0xa0, 0xb0, 0xb1, 0xa0, 0xc0, 0xa1, 0xc1, 0x78,
        0xa5, 0xb5, 0xb6, 0xa5, 0xc5, 0xa6, 0xc6, 0x89,
        0xf0
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_structs_array *pld2 = NULL;
    ret = TestPld_structs_array_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->nested[0].a, pld.nested[0].a);
    check_eq(pld2->nested[0].b, pld.nested[0].b);
    check_eq(pld2->nested[0].inner[0].a, pld.nested[0].inner[0].a);
    check_eq(pld2->nested[0].inner[0].c, pld.nested[0].inner[0].c);
    check_eq(pld2->nested[0].inner[1].a, pld.nested[0].inner[1].a);
    check_eq(pld2->nested[0].inner[1].c, pld.nested[0].inner[1].c);

    check_eq(pld2->nested[1].a, pld.nested[1].a);
    check_eq(pld2->nested[1].b, pld.nested[1].b);
    check_eq(pld2->nested[1].inner[0].a, pld.nested[1].inner[0].a);
    check_eq(pld2->nested[1].inner[0].c, pld.nested[1].inner[0].c);
    check_eq(pld2->nested[1].inner[1].a, pld.nested[1].inner[1].a);
    check_eq(pld2->nested[1].inner[1].c, pld.nested[1].inner[1].c);

    check_eq(pld2->single[0].f, pld.single[0].f);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
