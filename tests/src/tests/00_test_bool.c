#include "../test.h"

bool test_bool(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_bool pld = {
        .boolb = 1,
        .booll = 1,
        .b2 = 0,
        .b3 = 1
    };

    /* Pack */
    ret = TestPld_test_bool_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 4ul);

    /* Check packed format */
    const uint8_t expected[] = { 0x01, 0x01, 0x00, 0x01 };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_bool *pld2 = NULL;
    ret = TestPld_test_bool_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->boolb, pld.boolb);
    check_eq(pld2->booll, pld.booll);
    check_eq(pld2->b2, pld.b2);
    check_eq(pld2->b3, pld.b3);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
