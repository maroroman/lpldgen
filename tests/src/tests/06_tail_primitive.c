#include "../test.h"

bool test_tail_primitive(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_tail_primitive *pld = malloc(TESTPLD_TAIL_PRIMITIVE_C_SIZE(2));
    pld->pld_id = TESTPLD_TAIL_PRIMITIVE_ID; // malloc can have unallocated bytes: use calloc, or set ID!
    pld->tail_len = 2;
    pld->nontail = 0xABCD;
    pld->tail[0] = 123.25f;
    pld->tail[1] = 678.75f;

    /* Pack */
    ret = TestPld_tail_primitive_build(pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 10ul);

    /* Check packed format */
    const uint8_t expected[] = {
        0xAB, 0xCD,
        // little endian u32 (float*100)
        0x25, 0x30, 0x00, 0x00,
        0x23, 0x09, 0x01, 0x00,
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_tail_primitive *pld2 = NULL;
    ret = TestPld_tail_primitive_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq_ul(pld2->tail_len, 2ul);
    check_eq(pld2->nontail, pld->nontail);
    check_eq_f(pld2->tail[0], pld->tail[0]);
    check_eq_f(pld2->tail[1], pld->tail[1]);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
