#include "../test.h"

bool test_u32(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_u32 pld = {
        .u32b = 0xABCDEF01,
        .u32l = 0xABCDEF01,
    };

    /* Pack */
    ret = TestPld_test_u32_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 8ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0xAB, 0xCD, 0xEF, 0x01,
        // little
        0x01, 0xEF, 0xCD, 0xAB
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_u32 *pld2 = NULL;
    ret = TestPld_test_u32_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->u32b, pld.u32b);
    check_eq(pld2->u32l, pld.u32l);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
