#include "../test.h"

bool test_tail_struct(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_tail_struct *pld = malloc(TESTPLD_TAIL_STRUCT_C_SIZE(2));
    pld->pld_id = TESTPLD_TAIL_STRUCT_ID; // malloc can have unallocated bytes: use calloc, or set ID!
    pld->tail_len = 2;
    pld->tail[0].a = 10;
    pld->tail[0].b = 20;
    pld->tail[1].a = 30;
    pld->tail[1].b = 40;

    /* Pack */
    ret = TestPld_tail_struct_build(pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 4ul);

    /* Check packed format */
    const uint8_t expected[] = {
        10, 20, 30, 40,
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_tail_struct *pld2 = NULL;
    ret = TestPld_tail_struct_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq_ul(pld2->tail_len, 2ul);
    check_eq(pld->tail[0].a, pld2->tail[0].a);
    check_eq(pld->tail[0].b, pld2->tail[0].b);
    check_eq(pld->tail[1].a, pld2->tail[1].a);
    check_eq(pld->tail[1].b, pld2->tail[1].b);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
