#include "../test.h"

bool test_i32(void) {
    uint8_t *packed;
    size_t len;
    pld_error_t ret;

    /* Source data */
    struct TestPld_test_i32 pld = {
        .i32b = -2147483648, // 0x8000_0000
        .i32l = -456465465,  // 0xE4CA_E3C7
        .i32b2 = 2147483647, // 0x7FFF_FFFF
        .i32l2 = 305481351,  // 0x1235_4687
    };

    /* Pack */
    ret = TestPld_test_i32_build(&pld, malloc, &packed, &len);
    check(ret == PLD_OK);
    check_eq_ul(len, 16ul);

    /* Check packed format */
    const uint8_t expected[] = {
        // big
        0x80, 0x00, 0x00, 0x00,
        // little
        0xC7, 0xE3, 0xCA, 0xE4,
        // big
        0x7F, 0xFF, 0xFF, 0xFF,
        // little
        0x87, 0x46, 0x35, 0x12
    };
    check_array(packed, expected);

    /* Unpack */
    struct TestPld_test_i32 *pld2 = NULL;
    ret = TestPld_test_i32_parse(packed, len, malloc, &pld2);
    check(ret == PLD_OK);
    check(pld2 != NULL);

    /* Compare with original */
    check_eq(pld2->i32b, pld.i32b);
    check_eq(pld2->i32l, pld.i32l);
    check_eq(pld2->i32b2, pld.i32b2);
    check_eq(pld2->i32l2, pld.i32l2);

    /* Clean up */
    free(pld2);
    free(packed);
    return true;
}
