use std::collections::HashMap;
use std::path::PathBuf;

use crate::spec::field::Field;
use crate::spec::PayloadSpec;
use crate::utils;

const TEMPLATE_SEP : &str = "/";

#[derive(SmartDefault, Serialize, Deserialize, Debug)]
#[serde(default)]
pub(crate) struct ModuleConfig {
    /// Module name, used to name the output files and generated C symbols.
    /// If this module has no payload specs, then name, prefix and id_range
    /// have no effect.
    pub(crate) name: String,

    /// Build/parse and payload struct name prefix, acts as a namespace.
    /// Leave empty for no prefix.
    pub(crate) prefix: String,

    /// Paths to specification files and folders to include in the module
    pub(crate) specs: Vec<String>,

    /// Numeric range allocated to this module's payloads.
    /// These are values for the payload ID enum.
    pub(crate) id_range: Option<[u32; 2]>,

    /// Sub-modules to include (each with its own id_range and config)
    /// Prefix and name DO NOT stack!
    pub(crate) modules: Vec<String>,

    /// Variables (apply to own specs and override variables in sub-modules)
    pub(crate) variables: HashMap<String, u64>,
}

impl ModuleConfig {
    /// Perform a rough validation of the module config
    pub(crate) fn validate(&self) -> anyhow::Result<()> {
        if !self.specs.is_empty() {
            match &self.id_range {
                None => anyhow::bail!("id_range is required for modules containing payload specs!"),
                Some([a, b]) => {
                    if a > b {
                        anyhow::bail!("Invalid id_range {}..{}!", a, b);
                        // ID ranges will be checked for overlap later when all configs are parsed
                    }
                }
            }
            if self.id_range.is_none() {}

            if self.name.is_empty() {
                anyhow::bail!("name is required for modules containing payload specs!");
            }

            /* Prefix may be left empty */
        }

        /*
        If there are no specs, then id_range, prefix and name have no effect,
        but having them filled is not an error.
        */

        Ok(())
    }
}

#[derive(Clone, Debug, Default)]
pub(crate) struct SpecParseContext {
    variables: HashMap<String, u64>,
}

impl SpecParseContext {
    pub fn new() -> Self {
        Default::default()
    }

    /// Set variable if not exist
    pub fn set_variable(&mut self, name: &str, value: u64) {
        if name.starts_with(|c: char| c.is_numeric()) {
            config_error!(
                "Variable \"{}\" is illegal! Variable names may not start with a digit.",
                name
            );
        }

        if let Some(v) = self.variables.get(name).copied() {
            if v == value {
                debug!("Set variable \"{}\" = {} - NO CHANGE", name, v);
            } else {
                debug!(
                    "NOT setting \"{}\" to {} - already defined as {}",
                    name, value, v
                );
            }
        } else {
            debug!("Define variable \"{}\" = {}", name, value);
            self.variables.insert(name.to_string(), value);
        }
    }

    pub fn set_variables<'a>(&mut self, map: impl Iterator<Item = (&'a String, &'a u64)>) {
        for (k, v) in map {
            self.set_variable(k, *v);
        }
    }

    pub fn get_variable(&self, name: &str) -> Option<u64> {
        self.variables.get(name).copied()
    }
}

/// Parsed spec module
///
/// Sub-modules are loaded and appended to the main list.
/// Modules with no specs of their own are discarded.
#[derive(Debug)]
pub(crate) struct SpecModule {
    /// module options
    pub(crate) opts: ModuleConfig,
    /// Parsed specs
    pub(crate) specs: Vec<PayloadSpec>,
}

impl SpecModule {
    /// Apply substitutions to a string:
    ///
    /// Module name:
    /// - %module lowecase
    /// - %Module original
    /// - %MODULE uppercase
    pub(crate) fn substitute(&self, pattern: &str) -> String {
        pattern
            .replace("%module", &self.opts.name.to_ascii_lowercase())
            .replace("%Module", &self.opts.name)
            .replace("%MODULE", &self.opts.name.to_ascii_uppercase())
    }

    /// Apply substitutions to a string:
    ///
    /// Module name:
    /// - %module lowecase
    /// - %Module original
    /// - %MODULE uppercase
    ///
    /// Name (payload, struct, etc…):
    /// - %name lowecase
    /// - %Name original
    /// - %NAME uppercase
    pub(crate) fn substitute_name(&self, pattern: &str, name: &str) -> String {
        self.substitute(pattern)
            .replace("%name", &name.to_ascii_lowercase())
            .replace("%Name", name)
            .replace("%NAME", &name.to_ascii_uppercase())
    }
}

#[derive(Debug, Default)]
pub(crate) struct ModuleLoader {
    to_parse: Vec<(PathBuf, SpecParseContext)>,
}

impl ModuleLoader {
    pub(crate) fn new() -> Self {
        Default::default()
    }

    pub(crate) fn queue_modules_to_parse(
        &mut self,
        modules: &[String],
        basepath: Option<&PathBuf>,
        parse_context: &SpecParseContext,
    ) {
        for path_s in modules {
            debug!("New module path: {}", path_s);

            let path =
                utils::absolute_path(&path_s, basepath).expect("Failed to resolve input file path");

            if !path.is_dir() && !path_s.ends_with(".json") {
                config_error!(
                    "Path {} does not exist or is not a directory",
                    path.display()
                );
            }

            self.to_parse.push((path, parse_context.clone()));
        }
    }

    pub(crate) fn into_modules(mut self) -> anyhow::Result<Vec<SpecModule>> {
        let mut modules: Vec<SpecModule> = vec![];

        while !self.to_parse.is_empty() {
            let (mut modbase, mut parse_context) = self.to_parse.pop().unwrap();

            let modbase_s = modbase.display().to_string();

            info!("Parsing module at {}", modbase_s);

            let modfile = if modbase_s.ends_with(".json") {
                let file = modbase.clone();
                modbase = modbase.parent().unwrap().into();
                file
            } else {
                modbase.join("module.json")
            };

            if !modfile.is_file() {
                config_error!(
                    "Module file {} does not exist or is not a file!",
                    modfile.display()
                );
            }

            debug!("Module file is: {}", modfile.display());

            let text = utils::file_to_string(modfile)?;
            let modconf: ModuleConfig = json5::from_str(&text)?;

            modconf.validate()?;

            parse_context.set_variables(modconf.variables.iter());

            self.queue_modules_to_parse(&modconf.modules, Some(&modbase), &parse_context);

            if !modconf.specs.is_empty() {
                let spec_paths = modconf
                    .specs
                    .iter()
                    .map(|p| {
                        let ap = utils::absolute_path(p, Some(&modbase)).unwrap();

                        if !ap.is_file() {
                            config_error!("Bad spec path: {}", ap.display());
                        }

                        ap
                    })
                    .collect::<Vec<PathBuf>>();

                // Parsed specs
                let mut specs = vec![];
                for path in spec_paths {
                    let new_payloads = crate::file_to_payloads(&path, parse_context.clone())?;
                    specs.extend(new_payloads.into_iter());
                }

                modules.push(SpecModule {
                    opts: modconf,
                    specs,
                });
            }
        }

        apply_and_remove_templates(&mut modules)?;

        Ok(modules)
    }
}

fn apply_and_remove_templates(modules: &mut Vec<SpecModule>) -> anyhow::Result<()> {
    let mut templates = HashMap::new();

    // Remove all templates
    for m in modules.iter_mut() {
        let mut tpl_indices = vec![];
        m.specs.iter().enumerate().for_each(|(i, s)| {
            if s.tag.is_template() {
                tpl_indices.push(i);
            }
        });

        // reverse so we can efficiently swap-remove the specs
        tpl_indices.reverse();
        for i in tpl_indices {
            let t = m.specs.swap_remove(i);
            templates.insert(format!("{}{}{}", m.opts.name, TEMPLATE_SEP, t.name), t);
        }
    }

    // Apply templates
    for m in modules.iter_mut() {
        for s in &mut m.specs {
            apply_template_uses_in_spec(&m.opts.name, s, &templates)?;
        }
    }

    Ok(())
}

fn apply_template_uses_in_spec(
    this_module: &str,
    spec: &mut PayloadSpec,
    templates: &HashMap<String, PayloadSpec>,
) -> anyhow::Result<()> {
    apply_template_uses_in_vec(this_module, &mut spec.fields, templates)?;
    Ok(())
}

fn apply_template_uses_in_vec(
    this_module: &str,
    fields: &mut Vec<Field>,
    templates: &HashMap<String, PayloadSpec>,
) -> anyhow::Result<()> {
    let ff = std::mem::take(fields);
    let mut output = vec![];
    for f in ff {
        output.extend(apply_template_uses_in_field(this_module, f, templates)?);
    }
    *fields = output;
    Ok(())
}

fn apply_template_uses_in_field(
    this_module: &str,
    field: Field,
    templates: &HashMap<String, PayloadSpec>,
) -> anyhow::Result<Vec<Field>> {
    let mut output = vec![];
    match field {
        f @ Field::Simple { .. }
        | f @ Field::Const { .. }
        | f @ Field::Pad(_)
        | f @ Field::Bits { .. } => {
            output.push(f);
        }
        Field::Struct {
            mut fields,
            meta,
            name,
        } => {
            apply_template_uses_in_vec(this_module, &mut fields, templates)?;
            output.push(Field::Struct { fields, meta, name });
        }
        Field::Array { field, count } => {
            let mut ff = apply_template_uses_in_field(this_module, *field, templates)?;
            if ff.len() != 1 {
                anyhow::bail!("Array item cannot be expanded to multiple fields! Use a struct.");
            }
            output.push(Field::Array {
                count,
                field: Box::new(ff.remove(0)),
            });
        }
        Field::TailArray { field } => {
            let mut ff = apply_template_uses_in_field(this_module, *field, templates)?;
            if ff.len() != 1 {
                anyhow::bail!("Array item cannot be expanded to multiple fields! Use a struct.");
            }
            output.push(Field::TailArray {
                field: Box::new(ff.remove(0)),
            });
        }
        Field::Use {
            meta: use_meta,
            template: tpl_name,
        } => {
            let tpl_fullname = if tpl_name.contains(TEMPLATE_SEP) {
                tpl_name
            } else {
                format!("{}{}{}", this_module, TEMPLATE_SEP, tpl_name)
            };
            if let Some(tplspec) = templates.get(&tpl_fullname) {
                output = tplspec.fields.clone();
                // apply endian to sub-fields, if not individually overriden
                if let Some(ue) = use_meta.endian {
                    for f in &mut output {
                        if let Some(m) = f.get_meta_mut() {
                            if m.endian.is_none() {
                                m.endian = Some(ue);
                            }
                        }
                    }
                }
            } else {
                anyhow::bail!("Could not resolve teplate \"{}\"", tpl_fullname);
            }
        }
    }

    Ok(output)
}
