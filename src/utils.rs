use path_clean::PathClean;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};
use std::{env, io};

/// Make path absolute, using the current workdir as the base for relative paths.
/// Does not access filesystem.
pub fn absolute_path<P, Q>(path: P, current: Option<Q>) -> io::Result<PathBuf>
where
    P: AsRef<Path>,
    Q: AsRef<Path>,
{
    let path = path.as_ref();
    if path.is_absolute() {
        Ok(path.to_path_buf().clean())
    } else {
        match current {
            None => Ok(env::current_dir()?.join(path).clean()),
            Some(p) => Ok(p.as_ref().join(path).clean()),
        }
    }
}

/// Load file as string
pub fn file_to_string<P: AsRef<Path>>(path: P) -> anyhow::Result<String> {
    let mut file = File::open(path.as_ref())?;
    let mut buf = String::new();
    file.read_to_string(&mut buf)?;
    Ok(buf)
}
