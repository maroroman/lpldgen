use crate::errors::Error;
use crate::spec::Endian;
use std::convert::TryFrom;
use std::mem::transmute;

/// Data type for primitive fields
#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum Type {
    U8,
    U16,
    U24,
    U32,
    U64,

    I8,
    I16,
    I24,
    I32,
    I64,

    F32,
    F64,

    Bool,
}

/// Constant value for a type
#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TypeValue {
    U8(u8),
    U16(u16),
    U24(u32),
    U32(u32),
    U64(u64),
    I8(i8),
    I16(i16),
    I24(i32),
    I32(i32),
    I64(i64),
    F32(f32),
    F64(f64),
    Bool(bool),
}

impl TypeValue {
    /// Turn the constant into a byte vector in the given endian
    pub fn to_bytes(self, endian: Endian) -> Vec<u8> {
        let mut buf = vec![];
        match self {
            TypeValue::Bool(value) => {
                if value {
                    buf.push(1);
                } else {
                    buf.push(0);
                }
            }
            TypeValue::U8(value) => {
                buf.push(value);
            }
            TypeValue::U16(value) => {
                buf.extend_from_slice(&unsafe { transmute::<_, [u8; 2]>(value.to_be()) })
            }
            TypeValue::U32(value) => {
                buf.extend_from_slice(&unsafe { transmute::<_, [u8; 4]>(value.to_be()) })
            }
            TypeValue::U24(value) => {
                buf.extend_from_slice(&unsafe { transmute::<_, [u8; 4]>(value.to_be()) }[1..])
            }
            TypeValue::U64(value) => {
                buf.extend_from_slice(&unsafe { transmute::<_, [u8; 8]>(value.to_be()) })
            }
            TypeValue::I8(value) => buf.push(unsafe { transmute::<_, u8>(value) }),
            TypeValue::I16(value) => {
                buf.extend_from_slice(&unsafe { transmute::<_, [u8; 2]>(value.to_be()) })
            }
            TypeValue::I32(value) => {
                buf.extend_from_slice(&unsafe { transmute::<_, [u8; 4]>(value.to_be()) })
            }
            TypeValue::I24(value) => {
                buf.extend_from_slice(&unsafe { transmute::<_, [u8; 4]>(value.to_be()) }[1..])
            }
            TypeValue::I64(value) => {
                buf.extend_from_slice(&unsafe { transmute::<_, [u8; 8]>(value.to_be()) })
            }
            TypeValue::F32(value) => buf.extend_from_slice(&value.to_be_bytes()),
            TypeValue::F64(value) => buf.extend_from_slice(&value.to_be_bytes()),
        }

        // we built the vec in big endian
        if endian == Endian::Little {
            buf.reverse()
        }

        buf
    }

    /// Transmute / up-cast to u64
    pub fn to_u64(self) -> u64 {
        match self {
            TypeValue::Bool(value) => {
                if value {
                    1
                } else {
                    0
                }
            }
            TypeValue::U8(value) => value as u64,
            TypeValue::U16(value) => value as u64,
            TypeValue::U32(value) | TypeValue::U24(value) => value as u64,
            TypeValue::U64(value) => value as u64,
            TypeValue::I8(value) => (unsafe { transmute::<_, u8>(value) }) as u64,
            TypeValue::I16(value) => (unsafe { transmute::<_, u16>(value) }) as u64,
            TypeValue::I32(value) => (unsafe { transmute::<_, u32>(value) }) as u64,
            TypeValue::I24(value) => {
                let unsigned = (unsafe { transmute::<_, u32>(value) }) as u64;
                if unsigned & 0x80_0000 != 0 {
                    // sign extend
                    unsigned | 0xFF
                } else {
                    unsigned
                }
            }
            TypeValue::I64(value) => (unsafe { transmute::<_, u64>(value) }) as u64,
            TypeValue::F32(value) => value.to_bits() as u64,
            TypeValue::F64(value) => value.to_bits(),
        }
    }

    /// Transmute / up-cast to u64
    pub fn to_c_literal(self) -> String {
        match self {
            TypeValue::Bool(value) => if value { "1" } else { "0" }.to_string(),
            TypeValue::U8(value) => format!("{}", value),
            TypeValue::U16(value) => format!("{}", value),
            TypeValue::U32(value) | TypeValue::U24(value) => format!("{}", value),
            TypeValue::U64(value) => format!("{}", value),
            TypeValue::I8(value) => format!("{}", value),
            TypeValue::I16(value) => format!("{}", value),
            TypeValue::I32(value) => format!("{}", value),
            TypeValue::I24(value) => format!("{}", value),
            TypeValue::I64(value) => format!("{}", value),
            TypeValue::F32(value) => format!("{}", value),
            TypeValue::F64(value) => format!("{}", value),
        }
    }
}

impl Type {
    /// Get byte size
    pub fn byte_size(self) -> usize {
        match self {
            Type::U8 => 1,
            Type::U16 => 2,
            Type::U24 => 3,
            Type::U32 => 4,
            Type::U64 => 8,
            Type::I8 => 1,
            Type::I16 => 2,
            Type::I24 => 3,
            Type::I32 => 4,
            Type::I64 => 8,
            Type::F32 => 4,
            Type::F64 => 8,
            Type::Bool => 1,
        }
    }

    /// Get corresponding C type name
    pub fn c_name(self) -> &'static str {
        match self {
            Type::U8 => "uint8_t",
            Type::U16 => "uint16_t",
            Type::U24 => "uint32_t",
            Type::U32 => "uint32_t",
            Type::U64 => "uint64_t",
            Type::I8 => "int8_t",
            Type::I16 => "int16_t",
            Type::I24 => "int32_t",
            Type::I32 => "int32_t",
            Type::I64 => "int64_t",
            Type::F32 => "float",
            Type::F64 => "double",
            Type::Bool => "bool",
        }
    }

    /// Get short name, as used in spec defs
    pub fn short_name(self) -> &'static str {
        match self {
            Type::U8 => "u8",
            Type::U16 => "u16",
            Type::U24 => "u24",
            Type::U32 => "u32",
            Type::U64 => "u64",
            Type::I8 => "i8",
            Type::I16 => "i16",
            Type::I24 => "i24",
            Type::I32 => "i32",
            Type::I64 => "i64",
            Type::F32 => "f32",
            Type::F64 => "f64",
            Type::Bool => "bool",
        }
    }

    //        /// Get corresponding C type's default value as a string (used for C gen)
    //        pub fn c_default_as_str(&self) -> &'static str {
    //            match self {
    //                Type::U8 => "0",
    //                Type::U16 => "0",
    //                Type::U24 => "0",
    //                Type::U32 => "0",
    //                Type::U64 => "0",
    //                Type::I8 => "0",
    //                Type::I16 => "0",
    //                Type::I24 => "0",
    //                Type::I32 => "0",
    //                Type::I64 => "0",
    //                Type::F32 => "0.0f",
    //                Type::F64 => "0.0",
    //                Type::Bool => "false",
    //            }
    //        }

    /// Get the unsigned version of this type, if available
    pub fn as_unsigned(self) -> Type {
        match self {
            Type::I8 => Type::U8,
            Type::I16 => Type::U16,
            Type::I24 => Type::U24,
            Type::I32 => Type::U32,
            Type::I64 => Type::U64,
            Type::F32 => Type::U32,
            Type::F64 => Type::U64,
            Type::Bool => Type::U8,
            _ => self,
        }
    }

    /// Check if this is a floating type
    pub fn is_floating(self) -> bool {
        self == Type::F32 || self == Type::F64
    }

    /// Check if this is a signed type (not floating)
    pub fn is_signed(self) -> bool {
        [Type::I8, Type::I16, Type::I24, Type::I32, Type::I64].contains(&self)
    }
}

/// Convert TypeValue to the corresponding Type
impl From<TypeValue> for Type {
    /// Extract type from a TypeValue
    fn from(tv: TypeValue) -> Self {
        Type::from(&tv)
    }
}

/// Convert a borrowed TypeValue to the corresponding Type
impl From<&TypeValue> for Type {
    /// Extract type from a borrowed TypeValue
    fn from(tv: &TypeValue) -> Self {
        match tv {
            TypeValue::U8(_) => Type::U8,
            TypeValue::U16(_) => Type::U16,
            TypeValue::U24(_) => Type::U24,
            TypeValue::U32(_) => Type::U32,
            TypeValue::U64(_) => Type::U64,
            TypeValue::I8(_) => Type::I8,
            TypeValue::I16(_) => Type::I16,
            TypeValue::I24(_) => Type::I24,
            TypeValue::I32(_) => Type::I32,
            TypeValue::I64(_) => Type::I64,
            TypeValue::F32(_) => Type::F32,
            TypeValue::F64(_) => Type::F64,
            TypeValue::Bool(_) => Type::Bool,
        }
    }
}

impl From<Type> for TypeValue {
    /// Create a TypeValue from Type
    /// This is essentially a tagged Default::default
    fn from(ty: Type) -> Self {
        TypeValue::from(&ty)
    }
}

impl From<&Type> for TypeValue {
    /// Create a TypeValue from borrowed Type
    /// This is essentially a tagged Default::default
    fn from(ty: &Type) -> Self {
        match ty {
            Type::U8 => TypeValue::U8(0),
            Type::U16 => TypeValue::U16(0),
            Type::U24 => TypeValue::U24(0),
            Type::U32 => TypeValue::U32(0),
            Type::U64 => TypeValue::U64(0),
            Type::I8 => TypeValue::I8(0),
            Type::I16 => TypeValue::I16(0),
            Type::I24 => TypeValue::I24(0),
            Type::I32 => TypeValue::I32(0),
            Type::I64 => TypeValue::I64(0),
            Type::F32 => TypeValue::F32(0f32),
            Type::F64 => TypeValue::F64(0f64),
            Type::Bool => TypeValue::Bool(false),
        }
    }
}

impl TryFrom<&str> for Type {
    type Error = Error<'static>;

    /// Convert from a name as used in the specification file
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Ok(match s {
            // Type name aliases are allowed to make spec writing easier
            "u8" | "uint8_t" | "uint8" | "byte" => Type::U8,
            "u16" | "uint16_t" | "uint16" => Type::U16,
            "u24" | "uint24_t" | "uint24" => Type::U24,
            "u32" | "uint32_t" | "uint32" => Type::U32,
            "u64" | "uint64_t" | "uint64" => Type::U64,

            "char" | "i8" | "int8_t" | "int8" => Type::I8,
            "i16" | "int16_t" | "int16" => Type::I16,
            "i24" | "int24_t" | "int24" => Type::I24,
            "i32" | "int32_t" | "int32" => Type::I32,
            "i64" | "int64_t" | "int64" => Type::I64,

            "f32" | "float" => Type::F32,
            "f64" | "double" => Type::F64,

            "bool" | "boolean" => Type::Bool,

            _ => {
                return Err(Error::from(format!("Unexpected data type name: {}", s)));
            }
        })
    }
}

#[cfg(test)]
mod tests {
    use super::TypeValue;
    use crate::spec::Endian;

    #[test]
    fn test_type_value_to_bytes() {
        // Bool
        assert_eq!(vec![0], TypeValue::Bool(false).to_bytes(Endian::Big));
        assert_eq!(vec![0], TypeValue::Bool(false).to_bytes(Endian::Little));
        assert_eq!(vec![1], TypeValue::Bool(true).to_bytes(Endian::Big));
        assert_eq!(vec![1], TypeValue::Bool(true).to_bytes(Endian::Little));
        // U8
        assert_eq!(vec![0xFF], TypeValue::U8(0xFF).to_bytes(Endian::Big));
        assert_eq!(vec![0x7F], TypeValue::U8(0x7F).to_bytes(Endian::Little));
        // I8
        assert_eq!(vec![0xFF], TypeValue::I8(-1).to_bytes(Endian::Big));
        assert_eq!(vec![0xFE], TypeValue::I8(-2).to_bytes(Endian::Big));
        assert_eq!(vec![123], TypeValue::I8(123).to_bytes(Endian::Little));
        //U16
        assert_eq!(
            vec![0xFF, 0xAF],
            TypeValue::U16(0xFFAF).to_bytes(Endian::Big)
        );
        assert_eq!(
            vec![0xAF, 0xFF],
            TypeValue::U16(0xFFAF).to_bytes(Endian::Little)
        );
        //I16
        assert_eq!(
            vec![0xFB, 0x2E],
            TypeValue::I16(-1234).to_bytes(Endian::Big)
        );
        assert_eq!(
            vec![0x2E, 0xFB],
            TypeValue::I16(-1234).to_bytes(Endian::Little)
        );
        //U32
        assert_eq!(
            vec![0xFF, 0xAF, 0x12, 0x34],
            TypeValue::U32(0xFFAF1234).to_bytes(Endian::Big)
        );
        assert_eq!(
            vec![0x34, 0x12, 0xAF, 0xFF],
            TypeValue::U32(0xFFAF1234).to_bytes(Endian::Little)
        );
        //I32
        assert_eq!(
            vec![0xFF, 0x43, 0x9E, 0xB2],
            TypeValue::I32(-12345678).to_bytes(Endian::Big)
        );
        assert_eq!(
            vec![0xB2, 0x9E, 0x43, 0xFF],
            TypeValue::I32(-12345678).to_bytes(Endian::Little)
        );
        //U64
        assert_eq!(
            vec![0xFF, 0xAF, 0x12, 0x34, 0x01, 0x23, 0x45, 0x67],
            TypeValue::U64(0xFFAF123401234567).to_bytes(Endian::Big)
        );
        assert_eq!(
            vec![0x67, 0x45, 0x23, 0x01, 0x34, 0x12, 0xAF, 0xFF],
            TypeValue::U64(0xFFAF123401234567).to_bytes(Endian::Little)
        );
        //I64
        assert_eq!(
            vec![0xFE, 0x49, 0x64, 0xB4, 0x53, 0x2F, 0xA0, 0xEB],
            TypeValue::I64(-123456789123456789).to_bytes(Endian::Big)
        );
        assert_eq!(
            vec![0xEB, 0xA0, 0x2F, 0x53, 0xB4, 0x64, 0x49, 0xFE],
            TypeValue::I64(-123456789123456789).to_bytes(Endian::Little)
        );
        //F32
        assert_eq!(
            vec![0xbf, 0x90, 0x00, 0x00],
            TypeValue::F32(-1.125f32).to_bytes(Endian::Big)
        );
        assert_eq!(
            vec![0x00, 0x00, 0x90, 0xbf],
            TypeValue::F32(-1.125f32).to_bytes(Endian::Little)
        );
        //F64
        assert_eq!(
            vec![0xC0, 0x28, 0xB0, 0xF2, 0x7B, 0xB2, 0xFE, 0xC5],
            TypeValue::F64(-12.3456f64).to_bytes(Endian::Big)
        );
        assert_eq!(
            vec![0xC5, 0xFE, 0xB2, 0x7B, 0xF2, 0xB0, 0x28, 0xC0],
            TypeValue::F64(-12.3456f64).to_bytes(Endian::Little)
        );
    }
}
