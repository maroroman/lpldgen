use core::cmp::Ordering;

pub mod tag;

pub mod field;
use crate::spec::field::{Field, TypeValue};
use crate::spec::tag::MsgTag;
pub use field::ByteMask;

/// Endian used for struct fields if not manually changed
pub const DEFAULT_ENDIAN: Endian = Endian::Big;

/// Configurable endianness
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Endian {
    /// big endian
    Big,
    /// little endian
    Little,
}

impl Endian {
    /// Check if this endian is Little
    #[allow(unused)]
    pub fn is_little(self) -> bool {
        self == Endian::Little
    }

    /// Check if this endian is High
    pub fn is_big(self) -> bool {
        self == Endian::Big
    }
}

/// Payload specification
#[derive(Debug, PartialEq, Clone)]
pub struct PayloadSpec {
    /// Payload name (used to name the outer struct in C)
    pub name: String,
    /// Description, used in debug dump output
    pub descr: Option<String>,
    /// Message identifying tag. There can be multiple messages with the same tag, so long as they
    /// can be distinguished by payload length or constant fields.
    pub tag: MsgTag,
    /// Payload endinanness on the wire
    pub endian: Endian,
    /// Exclude from recognizer, payload is for manual building and parsing only
    /// (this allows a conflicting specification)
    pub recognize: bool,
    /// Payload root struct fields
    pub fields: Vec<field::Field>,
}

impl PartialOrd for PayloadSpec {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(
            self.tag
                .cmp(&other.tag)
                .then_with(|| self.name.cmp(&other.name))
                .then_with(|| self.byte_length().cmp(&other.byte_length()))
                .then_with(|| self.get_constant_mask().cmp(&other.get_constant_mask())),
        )
    }
}

/// Parsed option (+name syntax) for PayloadSpec
#[derive(Debug, PartialEq)]
pub enum PayloadOption {
    Tag(MsgTag),
    Endian(Endian),
    Descr(String),
    Recognize(bool),
}

impl PayloadSpec {
    /// Build from a list of PayloadOption collected by the parser
    pub fn from_options(options: &[PayloadOption]) -> Self {
        let mut object = Self::default();

        for option in options {
            match option {
                PayloadOption::Tag(value) => object.tag = *value,
                PayloadOption::Endian(e) => object.endian = *e,
                PayloadOption::Descr(s) => match object.descr.as_mut() {
                    None => object.descr = Some(s.to_owned()),
                    Some(d) => {
                        *d += "\n";
                        *d += s;
                    }
                },
                PayloadOption::Recognize(flag) => object.recognize = *flag,
            }
        }
        object
    }

    /// Get minimal byte length
    pub fn byte_length(&self) -> usize {
        self.fields.iter().fold(0, |sum, f| sum + f.byte_length())
    }

    /// Get tuple of (name, len) for a tail array , if any.
    pub fn tail_array_info(&self) -> Option<(&str, usize)> {
        if let Some(Field::TailArray { field }) = self.fields.last() {
            let name = field.get_name().expect("Tail array must be named");
            let tail_elem_len = field.byte_length();
            Some((name, tail_elem_len))
        } else {
            None
        }
    }

    /// Check if the length is growable
    pub fn is_growable(&self) -> bool {
        self.fields.iter().any(|f| f.is_growable())
    }

    /// Check if the vec contains constants
    pub fn has_constants(&self) -> bool {
        self.fields.iter().any(|f| f.has_constants())
    }

    /// Check if the struct contains any variable fields
    pub fn has_variables(&self) -> bool {
        self.fields.iter().any(|f| f.has_variables())
    }

    /// Get the payload's constants mask - used to identify the payload among other
    /// same-length & address payloads
    pub fn get_constant_mask(&self) -> Option<Vec<ByteMask>> {
        field::fields_vec_to_const_mask(&self.fields, self.endian)
    }

    /// Get the payload's constants mask - used to identify the payload among other
    /// same-length & address payloads
    pub fn get_named_constants(&self) -> Vec<(&str, TypeValue)> {
        let mut names = Vec::new();
        for field in &self.fields {
            if let Some(m) = field.get_meta() {
                if let (Some(value), Some(name)) = (field.get_constant_value(), &m.constant_name) {
                    names.push((name.as_str(), value));
                }
            }
        }
        names
    }

    /// Check for duplicate fields, returns a duplicate field name
    pub fn check_for_duplicate_fields(&self) -> Option<String> {
        let mut names = Vec::<String>::new();
        for field in &self.fields {
            // Named field starts its own scope for sub-fields;
            // Anonymous field shares scope with this
            if let Some(n) = field.get_name() {
                if names.contains(&n.to_string()) {
                    return Some(n.to_string());
                } else {
                    names.push(n.to_string());

                    let inner = field.check_for_duplicate_fields(&mut vec![]);
                    if inner.is_some() {
                        return inner;
                    }
                }
            } else {
                let inner = field.check_for_duplicate_fields(&mut names);
                if inner.is_some() {
                    return inner;
                }
            }
        }

        None
    }
}

impl Default for PayloadSpec {
    fn default() -> Self {
        Self {
            name: String::default(),
            descr: None,
            tag: MsgTag::default(),
            endian: DEFAULT_ENDIAN,
            fields: Vec::new(),
            recognize: true,
        }
    }
}
