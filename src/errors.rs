use core::fmt;
use nom::error::{
    ContextError, ErrorKind, FromExternalError, ParseError, VerboseError, VerboseErrorKind,
};
use std::borrow::Cow;
use std::fmt::{Display, Formatter};
use std::io;
use std::num::ParseFloatError;

#[derive(Debug)]
pub enum Error<'a> {
    SpecParse(Box<SpecParseError<'a>>),
    Message(Cow<'a, str>),
    IoError(io::Error),
}

impl Display for Error<'_> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match &self {
            Error::SpecParse(ref e) => e.fmt(f),
            Error::Message(m) => write!(f, "Error: {}", m),
            Error::IoError(e) => write!(f, "IO Error: {}", e),
        }
    }
}

impl<'a> Error<'a> {
    pub fn get_message(&self) -> String {
        match self {
            Error::SpecParse(ref e) => match e.message {
                Some(ref c) => c.to_string(),
                None => format!("{}", e),
            },
            Error::Message(m) => m.to_string(),
            Error::IoError(e) => format!("{}", e),
        }
    }

    pub fn to_static(&self) -> Error<'static> {
        Error::Message(Cow::Owned(match self {
            Error::Message(m) => m.to_string(),
            _ => format!("{:#?}", self),
        }))
    }
}

impl std::error::Error for Error<'_> {}

impl<'a> From<SpecParseError<'a>> for Error<'a> {
    fn from(original: SpecParseError<'a>) -> Self {
        Error::SpecParse(Box::new(original))
    }
}

impl<'a> From<nom::Err<SpecParseError<'a>>> for Error<'a> {
    fn from(original: nom::Err<SpecParseError<'a>>) -> Self {
        match original {
            nom::Err::Incomplete(_) => unimplemented!(),
            nom::Err::Error(e) | nom::Err::Failure(e) => Error::SpecParse(Box::new(e)),
        }
    }
}

impl From<String> for Error<'_> {
    fn from(original: String) -> Self {
        Error::Message(original.into())
    }
}

impl From<std::io::Error> for Error<'_> {
    fn from(e: io::Error) -> Self {
        Error::IoError(e)
    }
}

impl From<&String> for Error<'_> {
    fn from(original: &String) -> Self {
        Error::Message(original.to_string().into())
    }
}

impl<'a> From<&'a str> for Error<'a> {
    fn from(original: &'a str) -> Self {
        Error::Message(original.into())
    }
}

// --------------------- spec parse error ---------------------------

/// Custom nom error with error message
#[derive(Debug, Clone, PartialEq)]
pub struct SpecParseError<'a> {
    pub nom_error: VerboseError<&'a str>,
    pub message: Option<Cow<'static, str>>,
}

impl<'a> FromExternalError<&'a str, ParseFloatError> for SpecParseError<'a> {
    fn from_external_error(input: &'a str, kind: ErrorKind, e: ParseFloatError) -> Self {
        Self {
            nom_error: VerboseError::add_context(
                input,
                "ParseFloatError",
                VerboseError::from_error_kind(input, kind),
            ),
            message: Some(e.to_string().into()),
        }
    }
}

impl<'a> SpecParseError<'a> {
    /// Construct with a custom error message
    pub fn new(input: &'a str, message: &'static str) -> Self {
        Self {
            nom_error: VerboseError::add_context(
                input,
                message,
                VerboseError::from_error_kind(input, ErrorKind::Verify),
            ),
            message: Some(message.into()),
        }
    }
}

impl Display for SpecParseError<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match &self.message {
            None => write!(f, "ParseError: {:#?}", self.nom_error),
            Some(m) => write!(f, "ParseError: {}, caused by: {:#?}", m, self.nom_error),
        }
    }
}

impl<'a> From<SpecParseError<'a>> for VerboseError<&'a str> {
    fn from(er : SpecParseError<'a>) -> VerboseError<&'a str> {
        er.nom_error
    }
}

// implement Error for our custom error
impl std::error::Error for SpecParseError<'_> {}

impl<'a> ParseError<&'a str> for SpecParseError<'a> {
    fn from_error_kind(input: &'a str, kind: ErrorKind) -> Self {
        Self {
            nom_error: VerboseError::from_error_kind(input, kind),
            message: None,
        }
    }

    fn append(input: &'a str, kind: ErrorKind, mut other: Self) -> Self {
        other
            .nom_error
            .errors
            .push((input, VerboseErrorKind::Nom(kind)));
        other
    }

    fn from_char(input: &'a str, c: char) -> Self {
        Self {
            nom_error: VerboseError::from_char(input, c),
            message: None,
        }
    }
}
