//! Shunting yard algorithm for arithmetic expression parsing

use super::ast::Node;
use super::tokenize::{Associativity, Op, Token};
use crate::errors::Error;

use Op::*;
use Token::*;

/// Shunting yard implementation
pub fn infix_to_postfix(input: Vec<Token>) -> Result<Vec<Token>, Error<'static>> {
    let mut output = vec![];
    let mut operators = vec![];

    for token in input.into_iter() {
        match token {
            Variable(_) | Literal(_) => {
                output.push(token);
            }
            Function(_) => {
                operators.push(token);
            }
            Operator(LParen) => {
                operators.push(token);
            }
            Operator(RParen) => {
                let mut found = false;
                while !operators.is_empty() {
                    let last = operators.pop().unwrap();
                    if let Operator(LParen) = last {
                        found = true;
                        break;
                    }

                    output.push(last);
                }

                if !found {
                    return Err(Error::from(
                        "Mismatched parentheses detected while finding opening paren",
                    ));
                }
            }
            Operator(ref op) => {
                while !operators.is_empty() {
                    let last = operators.last().unwrap();

                    match last {
                        Function(_) => {
                            output.push(operators.pop().unwrap());
                        }
                        Operator(LParen) => {
                            break;
                        }
                        Operator(last_op) => {
                            if (last_op.precedence() > op.precedence())
                                || (last_op.precedence() == op.precedence()
                                    && last_op.associativity() == Associativity::Left)
                            {
                                output.push(operators.pop().unwrap());
                            } else {
                                break;
                            }
                        }
                        _ => {
                            unreachable!("Unexpected token in operator stack: {:?}", last);
                        }
                    }
                }

                operators.push(token);
            }
        }
    }

    while !operators.is_empty() {
        let x = operators.pop().unwrap();
        match x {
            Operator(LParen) | Operator(RParen) => {
                return Err(Error::from(
                    "Mismatched parentheses found after parsing all input",
                ));
            }
            _ => {}
        }

        output.push(x);
    }

    Ok(output)
}

pub fn postfix_to_ast(postfix: Vec<Token>) -> Result<Node, Error<'static>> {
    let mut stack = vec![];
    for token in postfix.into_iter() {
        match token {
            Variable(n) => stack.push(Node::Variable(n)),
            Literal(v) => stack.push(Node::Literal(v)),
            Operator(op) => {
                if stack.len() < 2 {
                    return Err(Error::from(format!("Expected 2 operands to {:?}", op)));
                }

                // load in reverse order
                let (b, a) = (stack.pop().unwrap(), stack.pop().unwrap());
                let (a, b) = (Box::new(a), Box::new(b));
                stack.push(match op {
                    Add => Node::Add(a, b),
                    Sub => Node::Sub(a, b),
                    Mul => Node::Mul(a, b),
                    Div => Node::Div(a, b),
                    Mod => Node::Mod(a, b),
                    _ => {
                        return Err(Error::from(format!("Unexpected Op in postfix: {:?}", op)));
                    }
                });
            }
            Function(s) => {
                // all currently defined functions take one argument
                let a = stack.pop().unwrap();
                stack.push(match s.as_str() {
                    "round" => Node::Round(Box::new(a)),
                    "floor" => Node::Floor(Box::new(a)),
                    "ceil" => Node::Ceil(Box::new(a)),
                    "sin" => Node::Sin(Box::new(a)),
                    "asin" => Node::Asin(Box::new(a)),
                    "cos" => Node::Cos(Box::new(a)),
                    "acos" => Node::Acos(Box::new(a)),
                    "tan" => Node::Tan(Box::new(a)),
                    "atan" => Node::Atan(Box::new(a)),
                    _ => {
                        return Err(Error::from(format!(
                            "Missing Node variant for function {}",
                            s
                        )));
                    }
                });
            }
        }
    }

    if stack.len() != 1 {
        return Err(Error::from(format!(
            "Expected one remaining node on stack, found: {:?}",
            stack
        )));
    }

    Ok(stack.pop().unwrap())
}

#[cfg(test)]
mod tests {
    use super::Op::*;
    use super::Token::*;
    use crate::parser::expr::shyard::{infix_to_postfix, postfix_to_ast};
    use crate::parser::expr::tokenize::{token_vec_display, tokenize, unary_minus_fix};
    use crate::parser::expr::Node;

    #[test]
    fn test_infix_to_postfix() {
        let infix = vec![
            Literal(3f64),
            Operator(Add),
            Literal(4f64),
            Operator(Mul),
            Literal(2f64),
            Operator(Div),
            Operator(LParen),
            Literal(1f64),
            Operator(Sub),
            Literal(5f64),
            Operator(RParen),
        ];

        let postfix = infix_to_postfix(infix).unwrap();

        assert_eq!(
            vec![
                Literal(3f64),
                Literal(4f64),
                Literal(2f64),
                Operator(Mul),
                Literal(1f64),
                Literal(5f64),
                Operator(Sub),
                Operator(Div),
                Operator(Add),
            ],
            postfix
        );
    }

    #[test]
    fn test_infix_to_postfix2() {
        let infix = unary_minus_fix(tokenize("3 + 4 * 2 / ( 1 - 5 )").unwrap()).unwrap();
        let postfix = infix_to_postfix(infix).unwrap();
        assert_eq!("3 4 2 * 1 5 - / +", token_vec_display(&postfix));
    }

    #[test]
    fn test_postfix_to_ast() {
        let infix = unary_minus_fix(tokenize("1+2").unwrap()).unwrap();
        let postfix = infix_to_postfix(infix).unwrap();

        assert_eq!(
            Node::Add(Box::new(Node::Literal(1f64)), Box::new(Node::Literal(2f64)),),
            postfix_to_ast(postfix).unwrap(),
            "trivial test",
        );

        // test with precedence
        let infix = unary_minus_fix(tokenize("1+2*7").unwrap()).unwrap();
        let postfix = infix_to_postfix(infix).unwrap();

        assert_eq!(
            Node::Add(
                Box::new(Node::Literal(1f64)),
                Box::new(Node::Mul(
                    Box::new(Node::Literal(2f64)),
                    Box::new(Node::Literal(7f64)),
                )),
            ),
            postfix_to_ast(postfix).unwrap(),
            "test with precedence",
        );

        // test with parentheses
        let infix = unary_minus_fix(tokenize("(1+2)*7").unwrap()).unwrap();
        let postfix = infix_to_postfix(infix).unwrap();

        assert_eq!(
            Node::Mul(
                Box::new(Node::Add(
                    Box::new(Node::Literal(1f64)),
                    Box::new(Node::Literal(2f64)),
                )),
                Box::new(Node::Literal(7f64)),
            ),
            postfix_to_ast(postfix).unwrap(),
            "test with precedence",
        );
    }
}
