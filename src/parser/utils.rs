//! nom utils used in the config file parser and math parser

use std::num::ParseFloatError;

use nom::{alt, tag};
use nom::branch::alt;
use nom::bytes::complete::{
    escaped_transform, is_not, tag, take, take_until, take_while, take_while1,
};
use nom::character::complete::{anychar, multispace1, none_of, space1};
use nom::combinator::{map, map_res, not, opt, peek, recognize};
use nom::error::{ErrorKind, FromExternalError, ParseError};
use nom::IResult;
use nom::multi::{many0, many1};
use nom::sequence::{pair, preceded, terminated, tuple};

use crate::errors::SpecParseError;

/// Matches end of line
pub fn eol<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, &'a str, E> {
    if i.is_empty() {
        Ok((i, ""))
    } else {
        recognize(pair(opt(tag("\r")), tag("\n")))(i)
    }
}

/// Matches inline comment until the end of line, without the line terminator
pub fn inline_comment_unclosed<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E> {
    |input: &str| {
        recognize(preceded(
            alt((tag("#"), terminated(tag("//"), not(tag("/"))))),
            take_while(|x| x != '\n'),
        ))(input)
    }
}

/// Consume an in-line comment until end of the line or end of string
pub fn inline_comment<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E> {
    |input: &str| recognize(terminated(inline_comment_unclosed(), eol))(input)
}

/// Consume a multi-line comment /* ... */ that can be nested.
/// The comment must be closed
pub fn multiline_comment<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E> {
    |input: &str| {
        recognize(tuple((
            tag("/*"),
            many0(alt((
                // this allows nesting
                preceded(peek(tag("/*")), multiline_comment()),
                is_not("*"),
                preceded(tag("*"), is_not("/")),
            ))),
            take_until("*/"),
            take(2usize),
        )))(input)
    }
}

/// Eat up whitespace or line comment til EOL, including the \n.
/// Multi-line comment is not allowed here.
pub fn whitespace_to_eol<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E> {
    |input: &str| {
        recognize(terminated(
            many0(alt((
                space1,
                inline_comment_unclosed(),
                // This is problematic, because if the comment spans multiple lines,
                // it will consume the newline and it's possible that some valid tokens
                // will directly proceed. The parser should simply fail in that case
                multiline_comment(),
            ))),
            eol,
        ))(input)
    }
}

/// Eat up whitespace and comments until first non-white char, mandating at least one consumed
/// character
pub fn whitespace1<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E>
{
    |input: &str| {
        recognize(many1(alt((
            multispace1,
            multiline_comment(),
            inline_comment(),
        ))))(input)
    }
}

/// Eat up whitespace and comments until first non-white char
pub fn whitespace0<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E>
{
    |input: &str| {
        recognize(many0(alt((
            multispace1,
            multiline_comment(),
            inline_comment(),
        ))))(input)
    }
}

/// Wrap token in optional spaces /\s*()\s*/
pub fn space_around0<'a, O1, E, F>(mut first: F) -> impl FnMut(&'a str) -> IResult<&'a str, O1, E>
    where
    // Awful hacks to allow taking combinator as argument
        E: ParseError<&'a str>,
        F: FnMut(&'a str) -> IResult<&'a str, O1, E>,
{
    move |input: &'a str| {
        let (i1, _) = whitespace0()(input)?;
        let (i2, result) = first(i1)?;
        let (i2, _) = whitespace0()(i2)?;
        Ok((i2, result))
    }
}

/// Wrap token in optional and mandatory space /\s*()\s+/
pub fn space_around1<'a, O1, E, F>(mut first: F) -> impl FnMut(&'a str) -> IResult<&'a str, O1, E>
    where
        E: ParseError<&'a str>,
        F: FnMut(&'a str) -> IResult<&'a str, O1, E>,
//        nom::Err<E>: std::convert::From<nom::Err<(&'a str, nom::error::ErrorKind)>>
{
    move |input: &'a str| {
        let (i1, _) = whitespace0()(input)?;
        let (i2, result) = first(i1)?;
        let (i2, _) = whitespace1()(i2)?;
        Ok((i2, result))
    }
}

pub fn chars_with_filter<'a, E>(
    set: &'static str,
    ignore: &'static str,
) -> impl Fn(&'a str) -> IResult<&'a str, String, E>
    where
        E: ParseError<&'a str>,
//nom::Err<E>: std::convert::From<nom::Err<(&'a str, nom::error::ErrorKind)>>
{
    move |input| {
        let (input, res) = take_while1(|x| set.contains(x) || ignore.contains(x))(input)?;

        let cleaned = res.replace(|c| ignore.contains(c), "");

        if cleaned.is_empty() {
            // this can probably be done better - it's extra ugly because we implement this for
            // concrete type - &str
            parser_error!(input, "expected one of {} or (ignored) {}", set, ignore);
        } else {
            Ok((input, cleaned))
        }
    }
}

/// Parse a u64 number in any of the usual formats. Underscore is discarded.
pub fn u64_literal<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, u64, E> {
    |input: &str| {
        alt((
            map(
                preceded(tag("0x"), chars_with_filter("abcdefABCDEF0123456789", "_")),
                |s: String| {
                    if let Ok(num) = u64::from_str_radix(&s, 16) {
                        num
                    } else {
                        parser_error!(input, "Expected hex digits or underscores");
                        //Err(E::from_error_kind(input, ErrorKind::HexDigit))
                    }
                },
            ),
            map(
                preceded(tag("0b"), chars_with_filter("01", "_")),
                |s: String| {
                    if let Ok(num) = u64::from_str_radix(&s, 2) {
                        num
                    } else {
                        parser_error!(input, "Expected binary digits or underscores");
                        //                        Err(E::from_error_kind(input, ErrorKind::OneOf)) // TODO?
                    }
                },
            ),
            map(chars_with_filter("0123456789", "_"), |s: String| {
                if let Ok(num) = s.parse() {
                    num
                } else {
                    parser_error!(input, "Expected decimal digits or underscores");
                    //                    Err(E::from_error_kind(input, ErrorKind::Digit)) // TODO?
                }
            }),
        ))(input)
    }
}

/// Parse a singed u64 literal (i64)
pub fn i64_literal<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, i64, E> {
    |input: &str| {
        alt((
            map(pair(opt(tag("-")), u64_literal()), |(opt_minus, val)| {
                if opt_minus.is_some() {
                    -(val as i64)
                } else {
                    val as i64
                }
            }),
            map(
                terminated(
                    preceded(tag("'"), char_if(|c| (c as u32) >= 32 && (c as u32) < 127)),
                    tag("'"),
                ),
                |val| val as i64,
            ),
        ))(input)
    }
}

/// Parse a singed f64 literal
pub fn f64_literal<
    'a,
    E: FromExternalError<&'a str, ParseFloatError> + nom::error::ParseError<&'a str>,
>() -> impl FnMut(&'a str) -> IResult<&'a str, f64, E> {
    |input: &str| {
        map_res(
            recognize(tuple((
                opt(tag("-")),
                chars_with_filter("0123456789", "_"),
                opt(preceded(tag("."), chars_with_filter("0123456789", "_"))),
            ))),
            |seq| {
                let cleaned = seq.replace("_", "");
                cleaned.parse()
            },
        )(input)
    }
}

pub fn char_if<'a, T, E>(test: T) -> impl Fn(&'a str) -> IResult<&'a str, char, E>
    where
        T: Fn(char) -> bool,
        E: ParseError<&'a str>,
{
    move |i: &str| {
        let p = anychar(i)?;
        if test(p.1) {
            Ok(p)
        } else {
            Err(nom::Err::Error(E::from_error_kind(i, ErrorKind::Char)))
        }
    }
}

pub fn char_unless<'a, T, E: ParseError<&'a str>>(
    test: T,
) -> impl Fn(&'a str) -> IResult<&'a str, char, E>
    where
        T: Fn(char) -> bool,
{
    move |i: &str| {
        let p = anychar(i)?;
        if test(p.1) {
            Err(nom::Err::Error(E::from_error_kind(i, ErrorKind::Char)))
        } else {
            Ok(p)
        }
    }
}

pub fn identifier<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E>
{
    |i: &str| {
        recognize(pair(
            char_if(|c| c.is_ascii_alphabetic() || c == '_'),
            take_while(|c: char| c.is_ascii_alphanumeric() || c == '_'),
        ))(i)
    }
}

/// Identifier that supports some special characters after the first alpha/_.
pub fn identifier_extra<'a, E: ParseError<&'a str>>(extra: &'static str) -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E> {
    move |i: &str| {
        recognize(pair(
            char_if(|c| c.is_ascii_alphabetic() || c == '_'),
            take_while(|c: char| c.is_ascii_alphanumeric() || c == '_' || extra.contains(c)),
        ))(i)
    }
}

pub fn non_whitespace<'a, E: ParseError<&'a str>>() -> impl Fn(&'a str) -> IResult<&'a str, &'a str, E> {
    |i: &str| {
        recognize(many1(alt((
            // non-comment and non-white
            char_unless(|c| c == '#' || c == '/' || c.is_whitespace()),
            // or slash that's not a start of comment
            preceded(
                pair(peek(tag("/")), not(peek(alt((tag("//"), tag("/*")))))),
                anychar,
            ),
        ))))(i)
    }
}

/// Parse a quoted string that can be empty
pub fn quoted_string<'a, E: ParseError<&'a str>>() -> impl FnMut(&'a str) -> IResult<&'a str, String, E> {
    |input: &str| {
        map(
            terminated(
                preceded(
                    tag("\""),
                    opt(
                        // 'escaped_transform' requires at least one character, so we wrap it in opt and map None to empty String later
                        escaped_transform(none_of("\\\""), '\\', |i: &'a str| {
                            alt!(i,
                                tag!("\\") => { |_| "\\" } |
                                tag!("\"") => { |_| "\"" } |
                                tag!("'") => { |_| "\'" } |
                                tag!("t") => { |_| "\t" } |
                                tag!("n") => { |_| "\n" }
                            )
                        }),
                    ),
                ),
                tag("\""),
            ),
            |o| o.unwrap_or_default(),
        )(input)
    }
}

pub fn eof(input: &str) -> IResult<&str, (), SpecParseError> {
    if input.is_empty() {
        Ok(("", ()))
    } else {
        Err(nom::Err::Error(SpecParseError::new(
            input,
            "Expected end of file.",
        )))
    }
}

/// variant of map_res that doesnt discard the useful error message (wtf)
pub fn map_res2<'a, O1, O2, F, G>(
    mut first: F,
    mut second: G,
) -> impl FnMut(&'a str) -> IResult<&'a str, O2, SpecParseError<'a>>
    where
        F: FnMut(&'a str) -> IResult<&'a str, O1, SpecParseError<'a>>,
        G: FnMut(O1) -> Result<O2, nom::Err<SpecParseError<'a>>>,
{
    move |input: &'a str| {
        //let i = input.clone();
        let (input, o1) = first(input)?;
        match second(o1) {
            Ok(o2) => Ok((input, o2)),
            Err(e2) => Err(e2),
        }
    }
}

#[cfg(test)]
mod tests {
    use nom::bytes::complete::tag;
    use nom::combinator::recognize;
    use nom::error::ErrorKind;
    use nom::multi::many0;

    use crate::errors::SpecParseError;
    use crate::parser::utils::identifier_extra;

    use super::{
        char_if, char_unless, eol, f64_literal, i64_literal, identifier, inline_comment,
        inline_comment_unclosed, multiline_comment, quoted_string, space_around0, space_around1,
        u64_literal, whitespace0, whitespace1, whitespace_to_eol,
    };

    #[test]
    fn test_quoted_string() {
        let mut parser = quoted_string::<SpecParseError>();
        assert_eq!(Ok(("bubu", "".into())), parser("\"\"bubu"));
        assert_eq!(Ok(("", "".into())), parser("\"\""));
        assert_eq!(Ok(("", "aaa".into())), parser("\"aaa\""));
        assert_eq!(Ok(("", "aaa bbb ccc".into())), parser("\"aaa bbb ccc\""));
        assert_eq!(
            Ok(("xxx", "aaa\\bbb\"ccc".into())),
            parser("\"aaa\\\\bbb\\\"ccc\"xxx")
        );
    }

    #[test]
    fn test_char_unless() {
        assert_eq!(
            Ok(("*abcdef", "abcdef")),
            recognize(many0(char_unless::<_, SpecParseError>(|c| c == '*')))("abcdef*abcdef")
        );

        assert!(char_unless::<_, SpecParseError>(|c| c == 'a')("x").is_ok());
    }

    #[test]
    fn test_identifier() {
        let parser = identifier::<SpecParseError>();
        assert_eq!(Ok(("*abcdef", "abcdef")), parser("abcdef*abcdef"));
        assert_eq!(Ok((" aaa", "Abcdef")), parser("Abcdef aaa"));
        assert_eq!(Ok(("+aaa", "_xxx")), parser("_xxx+aaa"));
        assert!(parser("0aaa").is_err());
    }

    #[test]
    fn test_identifier_extra() {
        let parser = identifier_extra::<SpecParseError>("_.[]()");
        assert_eq!(Ok(("*abcdef", "abcdef")), parser("abcdef*abcdef"));
        assert_eq!(Ok((" aaa", "Abcdef")), parser("Abcdef aaa"));
        assert_eq!(Ok(("+aaa", "_xxx")), parser("_xxx+aaa"));
        assert!(parser("0aaa").is_err());

        assert_eq!(
            Ok(("*abcdef", "foo.bar[xx]_.[]()xx")),
            parser("foo.bar[xx]_.[]()xx*abcdef")
        );
    }

    #[test]
    fn test_char_if() {
        assert_eq!(
            Ok(("b", 'a')),
            char_if::<_, SpecParseError>(|c| c == 'a')("ab")
        );
        assert!(char_if::<_, SpecParseError>(|c| c == 'a')("x").is_err());
        assert_eq!(
            Ok(("123", "abcdef")),
            recognize(many0(char_if::<_, SpecParseError>(
                |c| c.is_ascii_alphabetic()
            )))("abcdef123")
        );
    }

    #[test]
    fn test_f64_literal() {
        let mut parser = f64_literal::<SpecParseError>();

        assert!(parser("").is_err(), "empty is not valid");
        assert_eq!(Ok(("", 123f64)), parser("123"), "Integer");
        assert_eq!(Ok(("", -123f64)), parser("-123"), "Integer negative");
        assert_eq!(Ok(("", 123.356f64)), parser("123.356"), "Positive");
        assert_eq!(Ok(("", -123.356f64)), parser("-123.356"), "Negative");
        assert_eq!(
            Ok(("", 123.356f64)),
            parser("12_3.35_6_"),
            "Positive with underscore"
        );
        assert_eq!(
            Ok(("", -123.356f64)),
            parser("-12_3.35_6_"),
            "Negative with underscore"
        );
    }

    #[test]
    fn test_u32_literal() {
        let parser = u64_literal::<SpecParseError>();

        assert!(parser("").is_err(), "empty is not valid");
        assert_eq!(Ok(("", 123)), parser("123"), "Simple dec");
        assert_eq!(Ok(("", 9)), parser("9"), "Simple dec 2");
        assert_eq!(Ok(("", 0xff00)), parser("0xff00"), "Simple hex");
        assert_eq!(Ok(("", 0xf)), parser("0xf"), "Single digit hex");
        assert_eq!(Ok(("", 0b1111_0000)), parser("0b11110000"), "Binary");
        assert_eq!(
            Ok(("", 0b1111_0000)),
            parser("0b1111_0000"),
            "Binary underscore"
        );
        assert_eq!(Ok(("", 0b01)), parser("0b01"), "Binary small");
        assert_eq!(Ok(("aaa", 0b01)), parser("0b01aaa"), "Binary small, trail");
        assert_eq!(Ok(("", 0xab123456)), parser("0xab123456"), "long hex");
        assert_eq!(
            Ok(("", 0xab123456)),
            parser("0xab12_3456"),
            "long hex with underscore"
        );
    }

    #[test]
    fn test_eol() {
        assert_eq!(Ok(("", "")), eol::<SpecParseError>(""), "empty is EOL");
        assert_eq!(
            Ok(("", "\n")),
            eol::<SpecParseError>("\n"),
            "\\n and EOL is EOL"
        );
        assert_eq!(
            Ok(("", "\r\n")),
            eol::<SpecParseError>("\r\n"),
            "\\r\\n and EOL is EOL"
        );
        assert_eq!(
            Ok(("xxx", "\n")),
            eol::<SpecParseError>("\nxxx"),
            "\\n and more text is EOL"
        );
        assert_eq!(
            Ok(("xxx", "\r\n")),
            eol::<SpecParseError>("\r\nxxx"),
            "\\r\\n and more text is EOL"
        );
        assert!(eol::<SpecParseError>("xxx").is_err(), "not eol");
    }

    #[test]
    fn test_inline_comment_unclosed() {
        let parser = inline_comment_unclosed::<SpecParseError>();
        assert_eq!(
            Ok(("", "#comment here \t ")),
            parser("#comment here \t "),
            "comment to EOF"
        );
        assert_eq!(
            Ok(("\nxxx", "#comment here")),
            parser("#comment here\nxxx"),
            "comment to \\n"
        );
        assert_eq!(
            Ok(("\nxxx", "#comment here \t \r")),
            parser("#comment here \t \r\nxxx"),
            "comment to \\r\\n"
        );
        assert_eq!(
            Ok(("", "//comment here \t ")),
            parser("//comment here \t "),
            "C++ style comment to EOF"
        );
        assert_eq!(
            Ok(("\nxx", "//comment here \t ")),
            parser("//comment here \t \nxx"),
            "C++ style comment to EOF"
        );
    }

    #[test]
    fn test_inline_comment() {
        let parser = inline_comment::<SpecParseError>();
        assert_eq!(
            Ok(("", "#comment here \t ")),
            parser("#comment here \t "),
            "comment to EOF"
        );
        assert_eq!(
            Ok(("xxx", "#comment here\n")),
            parser("#comment here\nxxx"),
            "comment to \\n"
        );
        assert_eq!(
            Ok(("xxx", "#comment here \t \r\n")),
            parser("#comment here \t \r\nxxx"),
            "comment to \\r\\n"
        );
        assert_eq!(
            Ok(("", "//comment here \t ")),
            parser("//comment here \t "),
            "C++ style comment to EOF"
        );
    }

    #[test]
    fn test_multiline_comment() {
        let parser = multiline_comment::<SpecParseError>();

        assert_eq!(
            Ok(("  more text", "/* multi\nline \t// comment*/")),
            parser("/* multi\nline \t// comment*/  more text"),
            "Simple multiline"
        );

        assert_eq!(
            Ok(("end of the line", "/* gah */")),
            parser("/* gah */end of the line"),
            "Multi line in-line"
        );

        assert_eq!(
            Ok(("end of the line", "/* foo /* bar\nbaz */ */")),
            parser("/* foo /* bar\nbaz */ */end of the line"),
            "Multi line nested 1"
        );

        assert_eq!(
            Ok(("end of the line", "/*/*/**/*/*/")),
            parser("/*/*/**/*/*/end of the line"),
            "Multi line nested 3"
        );

        assert!(parser("/*/").is_err(), "Unclosed multiline is error");
        assert!(parser("/*").is_err(), "Unclosed multiline is error 2");
        assert!(
            parser("not a comment /* xxx */").is_err(),
            "not a comment"
        );
        assert!(parser("/ * * /").is_err(), "not a comment 2");
        assert!(parser("// * * /").is_err(), "not a comment 3");
    }

    #[test]
    fn test_whitespace_to_eol() {
        let parser = whitespace_to_eol::<SpecParseError>();

        assert!(parser("xxx").is_err(), "not whitespace");
        assert!(parser("     x\n").is_err(), "not whitespace 2");

        assert_eq!(Ok(("", "")), parser(""), "empty - noop");

        assert_eq!(Ok(("", "   \t")), parser("   \t"), "spaces and tabs");

        assert_eq!(
            Ok(("xxx", "   \t  \n")),
            parser("   \t  \nxxx"),
            "spaces and tabs to \\n"
        );

        assert_eq!(
            Ok(("xxx", "   \t  \r\n")),
            parser("   \t  \r\nxxx"),
            "spaces and tabs to \\r\\n"
        );

        assert_eq!(
            Ok(("", "   \t #comment here \t ")),
            parser("   \t #comment here \t "),
            "comment to EOF"
        );

        assert_eq!(
            Ok(("xxx", "   \t #comment here\n")),
            parser("   \t #comment here\nxxx"),
            "comment to \\n"
        );

        assert_eq!(
            Ok(("xxx", "   \t #comment here \t \r\n")),
            parser("   \t #comment here \t \r\nxxx"),
            "comment to \\r\\n"
        );

        assert_eq!(
            Ok(("", "   \t //comment here \t ")),
            parser("   \t //comment here \t "),
            "C++ style comment to EOF"
        );

        assert_eq!(
            Ok(("aa", "   \t /* multi /* line */ xxx */ \t \n")),
            parser("   \t /* multi /* line */ xxx */ \t \naa"),
            "Multi-line comment in line whitespace"
        );

        assert_eq!(Ok(("aa", "   \t /* multi\nple\nlines\nlong\ncomment */ \t \n")),
                   parser("   \t /* multi\nple\nlines\nlong\ncomment */ \t \naa"),
                   "Multi-line with line breaks in inline whitespace is OK if there's still some endline to match on");

        assert_eq!(
            Ok(("xxx", " /* multi\nline */\n")),
            parser(" /* multi\nline */\nxxx"),
            "no whitespace past multiline comment in line whitespace, but a valid newline"
        );

        assert!(
            parser(" /* multi\nline */ xxx").is_err(),
            "non-white tokens following multiline comment in line whitespace"
        );
    }

    #[test]
    fn test_whitespace1() {
        let parser = whitespace1::<SpecParseError>();

        assert_eq!(
            Ok(("aaa", " \n\t\r\n")),
            parser(" \n\t\r\naaa"),
            "true whitespace"
        );
        assert_eq!(Ok(("aaa", " ")), parser(" aaa"), "single whitespace");
        assert_eq!(
            Ok(("aaa", "/**/")),
            parser("/**/aaa"),
            "comments count as whitespace"
        );
        assert!(parser("aaa").is_err(), "whitespace required!");

        assert_eq!(
            Ok(("aaa", " /* gag\naga */ \n//moo\n")),
            parser(" /* gag\naga */ \n//moo\naaa"),
            "whitespace with comments"
        );
    }

    #[test]
    fn test_whitespace0() {
        let parser = whitespace0::<SpecParseError>();

        assert_eq!(
            Ok(("aaa", " \n\t\r\n")),
            parser(" \n\t\r\naaa"),
            "true whitespace"
        );
        assert_eq!(Ok(("aaa", " ")), parser(" aaa"), "single whitespace");
        assert_eq!(Ok(("aaa", "")), parser("aaa"), "whitespace not required");
        assert_eq!(
            Ok(("aaa", " /* gag\naga */ \n//moo\n")),
            parser(" /* gag\naga */ \n//moo\naaa"),
            "whitespace with comments"
        );
    }

    #[test]
    fn test_space_around0() {
        let mut parser = space_around0(tag("xx"));

        assert_eq!(Ok(("", "xx")), parser("     xx  "));
        assert_eq!(Ok(("", "xx")), parser("xx"));
        assert_eq!(Ok(("bb", "xx")), parser(" \t xxbb"));
        assert_eq!(Ok(("aa", "xx")), parser("xx       \t \naa"));

        // this is required for type inference of the error type
        assert_eq!(parser(""), Err(nom::Err::Error(("", ErrorKind::Tag))));
    }

    #[test]
    fn test_space_around1() {
        let mut parser = space_around1(tag("xx"));

        assert_eq!(Ok(("", "xx")), parser("     xx  "));
        assert_eq!(Ok(("", "xx")), parser("xx "));
        assert_eq!(Ok(("bb", "xx")), parser("  xx bb"));
        assert!(parser("  xxbb").is_err());
        assert_eq!(Ok(("aa", "xx")), parser("xx       \t \naa"));

        // this is required for type inference of the error type
        assert_eq!(parser(""), Err(nom::Err::Error(("", ErrorKind::Tag))));
    }

    #[test]
    fn test_i64_literal() {
        let parser = i64_literal::<SpecParseError>();

        assert!(parser("").is_err(), "empty is not valid");
        assert_eq!(Ok(("", 123)), parser("123"), "Simple dec");
        assert_eq!(Ok(("", -123)), parser("-123"), "Simple dec neg");
        assert_eq!(Ok(("", 9)), parser("9"), "Simple dec 2");
        assert_eq!(Ok(("", -9)), parser("-9"), "Simple dec 2 neg");
        assert_eq!(Ok(("", 0xff00)), parser("0xff00"), "Simple hex");
        assert_eq!(Ok(("", 0xf)), parser("0xf"), "Single digit hex");
        assert_eq!(Ok(("", -0xff00)), parser("-0xff00"), "Neg hex");
        assert_eq!(Ok(("", 0b1111_0000)), parser("0b11110000"), "Binary");
        assert_eq!(
            Ok(("", 0b1111_0000)),
            parser("0b1111_0000"),
            "Binary underscore"
        );

        assert_eq!(Ok(("", 'A' as i64)), parser("'A'"), "ASCII");
        assert_eq!(Ok(("", '9' as i64)), parser("'9'"), "ASCII");

        assert_eq!(Ok(("", -0b1111_0000)), parser("-0b11110000"), "Binary neg");
        assert_eq!(Ok(("", 0b01)), parser("0b01"), "Binary small");
        assert_eq!(Ok(("aaa", 0b01)), parser("0b01aaa"), "Binary small, trail");
        assert_eq!(Ok(("", 0xab123456)), parser("0xab123456"), "long hex");
        assert_eq!(
            Ok(("", 0xab123456)),
            parser("0xab12_3456"),
            "long hex with underscore"
        );
        assert_eq!(
            Ok(("", -0xab123456)),
            parser("-0xab12_3456"),
            "long hex with underscore neg"
        );
    }
}
