use super::utils::Indent;
use super::utils::StructPath;
use super::utils::TempNamer;

use crate::spec::field::Type;
use crate::spec::PayloadSpec;

use ifmt::iformat;

use crate::codegen::packing_layout::{
    BitfieldPatternData, FieldPacking, PackingPattern, Transform,
};
use crate::codegen::utils::{PushString, ShiftLeftRight};
use crate::codegen::CodegenConfig;
use crate::modules::SpecModule;
use crate::parser::expr::ast::FloatPrecision;
use crate::parser::expr::Node;
use core::cmp::Ordering;
use std::borrow::Cow;

/// Build parser function prototypes (for the header)
pub(crate) fn build_parser_protos(
    config: &CodegenConfig,
    module: &SpecModule,
    specs: &[PayloadSpec],
) -> String {
    let mut items = Vec::new();

    for spec in specs {
        let fname = module.substitute_name(&config.tpl_parse_fn, &spec.name);
        let fname_s = module.substitute_name(&config.tpl_parse_static_fn, &spec.name);
        let stru = module.substitute_name(&config.tpl_payload_struct, &spec.name);
        let name = &spec.name;

        items.push(iformat!(r##"
            #if PLD_HAVE_ALLOC
            /** @brief Parse a binary payload into the "{name}" payload struct. */
            pld_error_t {fname}(const uint8_t *payload, size_t length, pld_alloc_fn alloc, struct {stru} **output);
            #endif /* PLD_HAVE_ALLOC */

            /** @brief Parse a binary payload into the "{name}" payload struct (static version) */
            pld_error_t {fname_s}(const uint8_t *payload, size_t length, struct {stru} *output);
            "##
        ).unindent());
    }

    items.join("\n\n")
}

/// Build parser functions - converting a binary payload to the data struct.
/// These functions are called when we already know it is the right payload variant.
pub(crate) fn build_parser_funcs(
    config: &CodegenConfig,
    module: &SpecModule,
    specs: &[PayloadSpec],
) -> String {
    let mut items = Vec::new();

    for spec in specs {
        let (code_dyn, code_static) = gen_payload_parser_code(config, module, spec);
        let code_dyn = code_dyn.indent_inner(5);
        let code_static = code_static.indent_inner(5);

        let fname = module.substitute_name(&config.tpl_parse_fn, &spec.name);
        let fname_s = module.substitute_name(&config.tpl_parse_static_fn, &spec.name);
        let stru = module.substitute_name(&config.tpl_payload_struct, &spec.name);

        items.push(
            iformat!(r#"
                #if PLD_HAVE_ALLOC
                pld_error_t {fname}(const uint8_t * bytes, const size_t length, pld_alloc_fn alloc, struct {stru} **output) {{
                    pld_log_trace("{fname}");
                    {code_dyn}
                }}
                #endif /* PLD_HAVE_ALLOC */

                pld_error_t {fname_s}(const uint8_t * bytes, const size_t length, struct {stru} *output) {{
                    pld_log_trace("{fname_s}");
                    {code_static}
                }}
                "#
            )
            .unindent(),
        );
    }

    items.join("\n\n")
}

// Placeholders for post-processing
const SKIP_BYTE: &str = "@skip_byte@";
const READ_BYTE: &str = "@read_byte@";
const ENDLOOP: &str = "@endloop@";
const READ_BYTE_NOINCREMENT: &str = "@read_byte_noinc@";
const SKIPLOOP_START: &str = "@skiploop_start@";
const SKIPLOOP_END: &str = "@skiploop_end@";

/// Build the body of a parser function, returns (dyn, static)
fn gen_payload_parser_code(
    config: &CodegenConfig,
    module: &SpecModule,
    spec: &PayloadSpec,
) -> (String, String) {
    let pld_len = spec.byte_length();
    let stru_name = module.substitute_name(&config.tpl_payload_struct, &spec.name);

    let mut code_header_dyn = String::new();

    if pld_len == 0 {
        code_header_dyn.push_str("(void)bytes;\n");
    }

    let mut code_header_s = code_header_dyn.clone();

    let f_pld_id = &config.pld_id_field;
    let f_binary_size = module.substitute_name(&config.tpl_binary_size_const, &spec.name);
    let pld_id = module.substitute_name(&config.tpl_enum_item, &spec.name);

    let f_c_size = module.substitute_name(&config.tpl_struct_c_size_const, &spec.name);

    let mut header_common = String::new();

    if spec.is_growable() {
        // The only way for it to be growable, currently, is to have the last field a TailArray
        let (tail_name, _) = spec.tail_array_info().expect("Tail info");
        let f_tail_len =
            module.substitute_name(&config.tpl_binary_tail_elem_size_const, &spec.name);

        if pld_len > 0 {
            header_common.push_string(
                iformat!(
                    r#"
                if (length < {f_binary_size}(0)) {{
                    pld_log_error("payload too short to parse");
                    return PLD_ERR_LENGTH;
                }}
                const size_t tail_size = length - {f_binary_size}(0);
                if ((tail_size % {f_tail_len}) != 0) {{
                    pld_log_error("tail array element incomplete");
                    return PLD_ERR_TAIL_LENGTH;
                }}
                const size_t tail_count = tail_size / {f_tail_len};
            "#
                )
                .unindent(),
            );
        } else {
            header_common.push_string(
                iformat!(
                    r#"
                /* this is a tail-only struct */
                if ((length % {f_tail_len}) != 0) {{
                    pld_log_error("tail array element incomplete");
                    return PLD_ERR_TAIL_LENGTH;
                }}
                const size_t tail_count = length / {f_tail_len};
            "#
                )
                .unindent(),
            );
        }

        // There is some code duplication, but that is needed

        code_header_dyn.push_string(format!("{}\n\n", header_common));
        code_header_dyn.push_string(
            iformat!(
                r#"
            struct {stru_name} *data = alloc({f_c_size}(tail_count));
            if (NULL == data) {{
                pld_log_error("payload struct alloc failed");
                return PLD_ERR_ALLOC;
            }}
            *output = data;
        "#
            )
            .unindent(),
        );

        code_header_s.push_string(
            iformat!(
                r#"
            if (!output) {{
                pld_log_error("output is NULL");
                return PLD_ERR_BAD_ARGS;
            }}
        "#
            )
            .unindent(),
        );
        code_header_s.push_string(format!("\n{}\n\n", header_common));
        code_header_s.push_string(
            iformat!(
                r#"
            output->{f_pld_id} = {pld_id};
            output->{tail_name}_len = tail_count;
        "#
            )
            .unindent(),
        );
    } else {
        header_common.push_string(
            iformat!(
                r#"
            if (length != {f_binary_size}) return PLD_ERR_LENGTH;
        "#
            )
            .unindent(),
        );

        // Not growable
        code_header_dyn.push_string(
            iformat!(
                r#"
            {header_common}
            if (!output) {{
                pld_log_error("output is NULL");
                return PLD_ERR_BAD_ARGS;
            }}
            struct {stru_name} *data = alloc({f_c_size});
            if (NULL == data) {{
                pld_log_error("payload struct alloc failed");
                return PLD_ERR_ALLOC;
            }}
            *output = data;
        "#
            )
            .unindent(),
        );

        code_header_s.push_string(
            iformat!(
                r#"
            {header_common}

            if (!output) {{
                pld_log_error("output is NULL");
                return PLD_ERR_BAD_ARGS;
            }}

            if (!bytes) {{
                pld_log_error("buffer is NULL");
                return PLD_ERR_BAD_ARGS;
            }}

            output->{f_pld_id} = {pld_id};
        "#
            )
            .unindent(),
        );
    }

    let mut gen_carry = ParsingGenCarry {
        tempnamer: TempNamer::new(),
    };

    let mut code_parsing = String::new();
    for field in &spec.fields {
        let path = StructPath::new("output", true);
        code_parsing.push_string(
            field
                .get_packing(&path, &mut gen_carry.tempnamer, spec.endian)
                .build_parsing_code("bytes", &mut gen_carry)
                + "\n\n",
        );
    }

    trace!("Raw generated parsing code:\n{}", code_parsing);

    // ugly hacks to fix "dead code"
    loop {
        code_parsing = code_parsing.trim().to_owned();
        if code_parsing.ends_with(SKIP_BYTE) {
            code_parsing = (&code_parsing[0..code_parsing.len() - SKIP_BYTE.len()]).to_string();
            continue;
        }

        let last_byte_read = code_parsing.rfind(READ_BYTE);
        let last_skiploop_start = code_parsing.rfind(SKIPLOOP_START).unwrap_or_default();
        let last_skiploop_end = code_parsing.rfind(SKIPLOOP_END).unwrap_or_default();
        let last_endloop = code_parsing.rfind(ENDLOOP).unwrap_or_default();
        if last_skiploop_end > last_endloop {
            // last loop is skiploop
            if let Some(last_read) = last_byte_read {
                if last_skiploop_end > last_read {
                    // and the last skiploop is after all reads
                    trace!("Skip trailing skiploop!");
                    code_parsing = (&code_parsing[0..last_skiploop_start]).to_string();
                    continue;
                }
            }
        }

        break;
    }

    let last_endloop = code_parsing.rfind(ENDLOOP).unwrap_or_default();
    let last_byte_read = code_parsing.rfind(READ_BYTE);

    trace!("Trimmed parsing code:\n{}", code_parsing);
    trace!(
        "Last brace: {}, last_read: {:?}",
        last_endloop,
        last_byte_read
    );

    if let Some(last_read) = last_byte_read {
        // both skip and read
        if last_read > last_endloop {
            // eligible for increment removal
            code_parsing.replace_range(
                last_read..last_read + READ_BYTE.len(),
                READ_BYTE_NOINCREMENT,
            );
        }
    }

    trace!("After patching:\n{}", code_parsing);

    // convert placeholders to C
    code_parsing = code_parsing
        .replace(SKIP_BYTE, "bytes++;")
        .replace(READ_BYTE, "*bytes++")
        .replace(READ_BYTE_NOINCREMENT, "*bytes")
        .replace(ENDLOOP, "")
        .replace(SKIPLOOP_START, "")
        .replace(SKIPLOOP_END, ""); // this is just a marker

    trace!("After subs:\n{}", code_parsing);

    gen_carry.tempnamer.use_bitfield_buf = false; // don't generate the 'bytebuf' declaration in parser code

    let tmp_decls = gen_carry.tempnamer.c_declarations();

    let mut buf_dyn = String::new();
    let mut buf_static = String::new();

    if !tmp_decls.is_empty() {
        buf_static.push_str(tmp_decls.trim());
        buf_static.push('\n');
    }

    buf_dyn.push_str(code_header_dyn.trim());
    buf_dyn.push('\n');

    buf_static.push_str(code_header_s.trim());
    buf_static.push('\n');

    if !code_parsing.is_empty() {
        buf_static.push_str("\n/* parsing */\n");
        buf_static.push_str(code_parsing.trim());
        buf_static.push('\n');
    }

    let fn_static = module.substitute_name(&config.tpl_parse_static_fn, &spec.name);
    buf_dyn.push_str(&iformat!("\nreturn {fn_static}(bytes, length, data);"));
    buf_static.push_str("\nreturn PLD_OK;");

    (buf_dyn, buf_static)
}

struct ParsingGenCarry {
    //    bitfield_members_to_clear : IndexMap<String, Type>,
    tempnamer: TempNamer,
}

trait FieldParsingCodeGen {
    fn build_parsing_code(&self, bytes_var: &str, carry: &mut ParsingGenCarry) -> String;
}

impl FieldParsingCodeGen for Vec<PackingPattern> {
    fn build_parsing_code(&self, bytes_var: &str, carry: &mut ParsingGenCarry) -> String {
        self.iter()
            .map(|p| p.build_parsing_code(bytes_var, carry))
            .filter(|s| !s.is_empty())
            .collect::<Vec<_>>()
            .join("\n")
    }
}

impl FieldParsingCodeGen for PackingPattern {
    fn build_parsing_code(&self, bytes_var: &str, carry: &mut ParsingGenCarry) -> String {
        let transmute_from_unsigned = |name: &str, dst: Type| match dst {
            Type::U8 | Type::U16 | Type::U24 | Type::U32 | Type::U64 => {
                iformat!("({dst.c_name()})({name})")
            }
            Type::I8 => iformat!("((x8_t){{ .u=(int8_t)({name}) }}).i"),
            Type::I16 => iformat!("((x16_t){{ .u=({name}) }}).i"),
            Type::I24 | Type::I32 => iformat!("((x32_t){{ .u=({name}) }}).i"),
            Type::I64 => iformat!("((x64_t){{ .u=({name}) }}).i"),
            Type::F32 => iformat!("((x32_t){{ .u=({name}) }}).f"),
            Type::F64 => iformat!("((x64_t){{ .u=({name}) }}).f"),
            Type::Bool => iformat!("((bool)({name}))"),
        };

        let render_transform = |tmpvar: &str, transform: &Transform, binary_ty: Type| {
            let floats = if transform.ty_repr == Type::F64 {
                FloatPrecision::Double
            } else {
                FloatPrecision::Float
            };

            let round = ![Type::F64, Type::F32].contains(&transform.ty_repr);

            let want = Node::Variable("x".to_string());

            let extract_name = if binary_ty.is_floating() {
                if transform.ty_repr == Type::F64 && binary_ty == Type::F32 {
                    transmute_from_unsigned(tmpvar, Type::F32)
                } else if transform.ty_repr == Type::F32 && binary_ty == Type::F64 {
                    transmute_from_unsigned(tmpvar, Type::F64)
                } else {
                    transmute_from_unsigned(tmpvar, transform.ty_repr)
                }
            } else if binary_ty.is_signed() {
                transmute_from_unsigned(tmpvar, binary_ty)
            } else {
                tmpvar.to_string()
                //iformat!("({transform.ty_repr.c_name()}) ({tmpvar})")
            };

            if transform.is_trivial() {
                // no explicit formula
                let cast = transform.ty_repr.c_name();
                iformat!("({cast}) {extract_name}")
            } else {
                let extract = Node::Variable(extract_name);

                let extracted = transform
                    .convert
                    .clone()
                    .extract(&want, extract)
                    .expect("Error reverse formula");

                let cast = transform.ty_repr.c_name();
                let rendered = extracted.render_c(floats, round);
                iformat!("({cast}) {rendered}")
            }
        };

        match self {
            PackingPattern::Byte {
                name,
                ty,
                transform, // TODO
            } => {
                if let Some(tr) = transform {
                    let tmp = carry.tempnamer.tmp_bytes(ty.as_unsigned().byte_size());
                    let c_calc = render_transform(tmp, tr, *ty);
                    iformat!("{tmp} = @read_byte@;\n{name} = {c_calc};")
                } else {
                    let c = transmute_from_unsigned("@read_byte@", *ty);
                    iformat!("{name} = {c};")
                }
            }
            PackingPattern::ByteConst(_) => "@skip_byte@".to_string(),
            PackingPattern::MultiByte {
                name,
                ty,
                bytes,
                transform, // TODO
            } => {
                let tmp = carry.tempnamer.tmp_bytes(ty.as_unsigned().byte_size());
                let mut buf = iformat!("{tmp} = 0;\n");

                let inner = bytes
                    .iter()
                    .map(|pos| {
                        let tyu = ty.as_unsigned();
                        if *pos == 0 {
                            iformat!("{tmp} |= ({tyu.c_name()}) @read_byte@;")
                        } else {
                            iformat!("{tmp} |= ({tyu.c_name()}) @read_byte@ << {pos*8};")
                        }
                    })
                    .collect::<Vec<_>>()
                    .join("\n");

                buf.push_string(inner);

                // sign extend for i24
                if *ty == Type::I24 {
                    buf.push('\n');
                    buf.push_string(
                        iformat!(
                            r#"
                        if ({tmp} & 0x800000) {{
                            {tmp} |= 0xFF000000;
                        }}"#
                        )
                        .unindent(),
                    );
                }

                buf.push('\n');

                buf + &if let Some(tr) = transform {
                    let c_calc = render_transform(tmp, tr, *ty);
                    iformat!("{name} = {c_calc};")
                } else {
                    let c = transmute_from_unsigned(tmp, *ty);
                    iformat!("{name} = {c};")
                }
            }
            PackingPattern::FixedRepetition {
                count,
                loop_var,
                patterns,
            } => {
                // Optimization to strip trailing paddings
                let (sls, sle) =
                    if *count < 16 && patterns.len() == 1 && patterns[0].is_byte_const() {
                        (SKIPLOOP_START, SKIPLOOP_END)
                    } else {
                        ("", "")
                    };

                let inner = patterns
                    .build_parsing_code(bytes_var, carry)
                    .indent_inner(6);

                iformat!(
                    r#"
                    {sls}for ({loop_var} = 0; {loop_var} < {count}; {loop_var}++) {{
                        {inner}
                    }}@endloop@{sle}
                "#
                )
                .unindent()
            }
            PackingPattern::DynamicRepetition {
                count_var,
                loop_var,
                patterns,
            } => {
                let inner = patterns
                    .build_parsing_code(bytes_var, carry)
                    .indent_inner(6);

                iformat!(
                    r#"
                    for ({loop_var} = 0; {loop_var} < {count_var}; {loop_var}++) {{
                        {inner}
                    }}@endloop@
                "#
                )
                .unindent()
            }
            PackingPattern::BitfieldByte { patterns } => {
                let tmp = carry.tempnamer.tmp_bytes(1);
                let mut buf = iformat!("{tmp} = @read_byte@;");
                let mut any = false;

                for p in patterns {
                    if let BitfieldPatternData::Field { name, ty, ty_cast } = &p.data {
                        any = true;

                        //                        if p.partial { // bool is inited by the one bit we parse
                        //                            carry.bitfield_members_to_clear.insert(name.clone(), *ty);
                        //                        }

                        let shifted_mask = p.mask.shift(p.rshift);

                        let s_shifting: Cow<'static, str> = match p.rshift.cmp(&0) {
                            Ordering::Less => iformat!(" >> {-p.rshift}").into(),
                            Ordering::Equal => "".into(),
                            Ordering::Greater => iformat!(" << {p.rshift}").into(),
                        };

                        let s_assign = if p.partial {
                            iformat!("{name} |= ({ty_cast.c_name()})")
                        } else {
                            iformat!("{name} = ({ty.c_name()})")
                        };

                        buf +=
                            &iformat!("\n{s_assign} (({tmp} & {shifted_mask:#04x}){s_shifting});");
                    }
                }

                if any {
                    buf
                } else {
                    "".to_string()
                }
            }
        }
    }
}
