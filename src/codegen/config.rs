#[derive(SmartDefault, Serialize, Deserialize, Debug)]
#[serde(default)]
pub struct Config {
    /// Whether to emit static files in the output directory
    #[default = true]
    pub generate_static: bool,

    /// Generated library file name (suffixed with .c and .h)
    #[default = "pld_%module"]
    pub filename: String,

    /// Payload ID field placed at the beginning of all structs
    #[default = "pld_id"]
    pub pld_id_field: String,

    /// Types enum name
    #[default = "%Module_PayloadType"]
    pub tpl_enum: String,

    /// Types enum item name
    #[default = "%MODULE_%NAME_ID"]
    pub tpl_enum_item: String,

    /// Named constant
    #[default = "%MODULE_%NAME_%CONST"]
    pub tpl_named_const: String,

    /// Struct binary size const name
    #[default = "%MODULE_%NAME_BIN_SIZE"]
    pub tpl_binary_size_const: String,

    /// Struct binary tail size const name
    #[default = "%MODULE_%NAME_TAIL_ELEM_BIN_SIZE"]
    pub tpl_binary_tail_elem_size_const: String,

    /// Struct C size const name
    #[default = "%MODULE_%NAME_C_SIZE"]
    pub tpl_struct_c_size_const: String,

    /// Struct C tail size const name
    #[default = "%MODULE_%NAME_TAIL_ELEM_C_SIZE"]
    pub tpl_struct_tail_elem_c_size_const: String,

    /// SRC ADDR const name
    #[default = "%MODULE_%NAME_SRC_ADDR"]
    pub tpl_struct_src_addr_const: String,

    /// SRC PORT const name
    #[default = "%MODULE_%NAME_SRC_PORT"]
    pub tpl_struct_src_port_const: String,

    /// DST ADDR const name
    #[default = "%MODULE_%NAME_DST_ADDR"]
    pub tpl_struct_dst_addr_const: String,

    /// DST PORT const name
    #[default = "%MODULE_%NAME_DST_PORT"]
    pub tpl_struct_dst_port_const: String,

    /// Payload testing function name
    #[default = "%Module_%Name_test"]
    pub tpl_test_fn: String,

    /// Payload building function name
    #[default = "%Module_%Name_build"]
    pub tpl_build_fn: String,

    /// Payload building function name
    #[default = "%Module_%Name_build_s"]
    pub tpl_build_static_fn: String,

    /// Payload parsing function name
    #[default = "%Module_%Name_parse"]
    pub tpl_parse_fn: String,

    /// Payload parsing function name
    #[default = "%Module_%Name_parse_s"]
    pub tpl_parse_static_fn: String,

    /// Payload export function name (public)
    #[default = "%Module_%Name_to_json"]
    pub tpl_export_fn: String,

    /// Payload export function name (private, only the content)
    #[default = "%Module_%Name_to_json_inner"]
    pub tpl_inner_export_fn: String,

    /// Parsing struct name
    #[default = "%Module_%Name"]
    pub tpl_payload_struct: String,

    /// Parsing struct name
    #[default = "Payloads_%Module"]
    pub tpl_module_struct: String,

    /// Pointer to the payload specification
    #[default = "PldSpec_%Module_%Name"]
    pub tpl_spec_pointer: String,
}
