use crate::codegen::bitfield_split::split_bitfield_to_bytes;
use crate::codegen::utils::{ReverseIf, StructPath, TempNamer};
use crate::codegen::C_KEYWORDS;
use crate::parser::expr::{ast, Node};
use crate::spec::field::{BitfieldScalar, Field, FieldMeta, Type, TypeValue};
use crate::spec::Endian;
use core::mem;

pub struct Transform {
    /// Representation type (type used in the data struct)
    pub ty_repr: Type,
    /// Conversion between the binary type (ty) and the data struct type (ty_repr)
    pub convert: ast::Node,
}

impl Transform {
    /// Check if the conversion is just the "x" variable by itself (used as a placeholder)
    pub fn is_trivial(&self) -> bool {
        match &self.convert {
            Node::Variable(s) => s == "x",
            _ => false,
        }
    }
}

/// Struct field packing pattern
pub enum PackingPattern {
    /// Single byte
    Byte {
        /// Variable name (structural path)
        name: String,
        /// Variable data type
        ty: Type,
        /// Conversion between the binary type (ty) and the data type used in the parsed struct
        transform: Option<Transform>,
    },

    /// Single byte constant
    ByteConst(u8),

    /// Multi-byte variable
    /// (constant is encoded as a ByteConst sequence)
    MultiByte {
        /// Variable name (structural path)
        name: String,
        /// Variable data type
        ty: Type,
        /// Multi-byte bytes (byte indexes, 0 = LSB)
        bytes: Vec<u8>,
        /// Conversion between the binary type (ty) and the data type used in the parsed struct
        transform: Option<Transform>,
    },

    /// Fixed length array
    FixedRepetition {
        /// Array length
        count: usize,
        /// Loop variable name
        loop_var: &'static str,
        /// Repeated pattern(s)
        patterns: Vec<PackingPattern>,
    },

    /// Dynamic length array (tail array)
    DynamicRepetition {
        /// Variable name (structural path) with the length
        count_var: String,
        /// Loop variable name
        loop_var: &'static str,
        /// Repeated pattern(s)
        patterns: Vec<PackingPattern>,
    },

    /// One byte with sub-byte patterns
    BitfieldByte {
        /// Patterns in the byte
        patterns: Vec<BitfieldPattern>,
    },
}

impl PackingPattern {
    /// Returns true if this is a byte constant
    pub fn is_byte_const(&self) -> bool {
        matches!(self, PackingPattern::ByteConst(_))
    }
}

/// Bitfield chunk
#[derive(Debug)]
pub struct BitfieldPattern {
    /// Pattern source data
    pub data: BitfieldPatternData,
    /// Chunk's highest bit number (in the source u64)
    pub high_bit: u8,
    /// Chunk size (in the source u64)
    pub count: u8,
    /// Positioned mask (in the source u64)
    pub mask: u64,
    /// Right shift to add to the mask after and-ing
    pub rshift: i8,
    /// True if the field's data is split across byte boundary (multiple BitfieldByte's)
    pub partial: bool,
}

/// Data for a bitfield chunk
#[derive(Debug)]
pub enum BitfieldPatternData {
    /// Variable field
    Field {
        /// Field name (structural path)
        name: String,
        /// Field data type
        ty: Type,
        /// Field data type (cast, for byte extracting)
        ty_cast: Type,
    },
    /// Constant field
    Const(u64),
}

/// Get field packing patterns
pub trait FieldPacking {
    fn get_packing(
        &self,
        path: &StructPath,
        tempnamer: &mut TempNamer,
        endian: Endian,
    ) -> Vec<PackingPattern>;
}

impl FieldPacking for Field {
    fn get_packing(
        &self,
        path: &StructPath,
        tempnamer: &mut TempNamer,
        parent_endian: Endian,
    ) -> Vec<PackingPattern> {
        if let Some(n) = self.get_name() {
            if C_KEYWORDS.contains(&n) {
                structural_error!(
                    "Field \"{}\" name is a C keyword. Please change the name.",
                    path.child(n).to_string()
                );
            }
        }

        match self {
            Field::Use { .. } => {
                panic!("Template uses should be resolved by now");
            }
            Field::Simple { name, dtype, meta } => {
                let endian = meta.endian.unwrap_or(parent_endian);

                let transform = if meta.transform.is_some() || meta.repr.is_some() {
                    let tr = Transform {
                        ty_repr: meta.repr.unwrap_or(*dtype),
                        convert: meta
                            .transform
                            .as_ref()
                            .cloned()
                            .unwrap_or_else(|| ast::Node::Variable("x".to_string())),
                    };
                    Some(tr)
                } else {
                    None
                };

                let pat = match dtype {
                    Type::U8 | Type::I8 | Type::Bool => PackingPattern::Byte {
                        name: path.child(name).to_string(),
                        ty: *dtype,
                        transform,
                    },

                    Type::U16 | Type::I16 => PackingPattern::MultiByte {
                        name: path.child(name).to_string(),
                        ty: *dtype,
                        bytes: vec![0, 1].reverse_if(endian.is_big()),
                        transform,
                    },

                    Type::U32 | Type::I32 | Type::F32 => PackingPattern::MultiByte {
                        name: path.child(name).to_string(),
                        ty: *dtype,
                        bytes: vec![0, 1, 2, 3].reverse_if(endian.is_big()),
                        transform,
                    },

                    Type::U24 | Type::I24 => PackingPattern::MultiByte {
                        name: path.child(name).to_string(),
                        ty: *dtype,
                        bytes: vec![0, 1, 2].reverse_if(endian.is_big()),
                        transform,
                    },

                    Type::U64 | Type::I64 | Type::F64 => PackingPattern::MultiByte {
                        name: path.child(name).to_string(),
                        ty: *dtype,
                        bytes: vec![0, 1, 2, 3, 4, 5, 6, 7].reverse_if(endian.is_big()),
                        transform,
                    },
                };

                vec![pat]
            }
            Field::Const { value, meta } => {
                fn const_u8(v: u8) -> Vec<PackingPattern> {
                    vec![PackingPattern::ByteConst(v)]
                }

                fn const_u16(v: u16) -> Vec<PackingPattern> {
                    vec![
                        PackingPattern::ByteConst((v & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 8) & 0xFF) as u8),
                    ]
                }

                fn const_u32(v: u32) -> Vec<PackingPattern> {
                    vec![
                        PackingPattern::ByteConst((v & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 8) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 16) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 24) & 0xFF) as u8),
                    ]
                }

                fn const_u24(v: u32) -> Vec<PackingPattern> {
                    vec![
                        PackingPattern::ByteConst((v & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 8) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 16) & 0xFF) as u8),
                    ]
                }

                fn const_u64(v: u64) -> Vec<PackingPattern> {
                    vec![
                        PackingPattern::ByteConst((v & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 8) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 16) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 24) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 32) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 40) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 48) & 0xFF) as u8),
                        PackingPattern::ByteConst(((v >> 56) & 0xFF) as u8),
                    ]
                }

                let endian = meta.endian.unwrap_or(parent_endian);
                (match value {
                    TypeValue::U8(v) => const_u8(*v),
                    TypeValue::U16(v) => const_u16(*v),
                    TypeValue::U24(v) => const_u24(*v),
                    TypeValue::U32(v) => const_u32(*v),
                    TypeValue::U64(v) => const_u64(*v),
                    TypeValue::I8(v) => const_u8(unsafe { mem::transmute::<_, u8>(*v) }),
                    TypeValue::I16(v) => const_u16(unsafe { mem::transmute::<_, u16>(*v) }),
                    TypeValue::I32(v) => const_u32(unsafe { mem::transmute::<_, u32>(*v) }),
                    TypeValue::I24(v) => const_u24(unsafe { mem::transmute::<_, u32>(*v) }),
                    TypeValue::F32(v) => const_u32((*v).to_bits()),
                    TypeValue::I64(v) => const_u64(unsafe { mem::transmute::<_, u64>(*v) }),
                    TypeValue::F64(v) => const_u64((*v).to_bits()),
                    TypeValue::Bool(v) => const_u8(if *v { 1 } else { 0 }),
                })
                .reverse_if(endian.is_big())
            }
            Field::Pad(count) => {
                let pat = PackingPattern::FixedRepetition {
                    count: *count,
                    loop_var: tempnamer.loopvar(*count),
                    patterns: vec![PackingPattern::ByteConst(0)],
                };
                vec![pat]
            }
            Field::Struct {
                name,
                fields,
                meta: FieldMeta { endian, .. },
            } => {
                let subpath = path.child(name);
                fields
                    .iter()
                    .map(|f| f.get_packing(&subpath, tempnamer, endian.unwrap_or(parent_endian)))
                    .flatten()
                    .collect()
            }
            Field::Array { count, field } => tempnamer.descend(|tempnamer: &mut TempNamer| {
                let ivar = tempnamer.loopvar(*count);
                let pat = PackingPattern::FixedRepetition {
                    count: *count,
                    loop_var: ivar,
                    patterns: field.get_packing(
                        &path.add_index_for_children(ivar),
                        tempnamer,
                        parent_endian,
                    ),
                };
                vec![pat]
            }),
            Field::TailArray { field } => tempnamer.descend(|tempnamer: &mut TempNamer| {
                let ivar = tempnamer.loopvar(std::u32::MAX as usize);
                let len_var = path
                    .child(&format!(
                        "{}_len",
                        field.get_name().expect("Tail array must have name")
                    ))
                    .to_string();
                let pat = PackingPattern::DynamicRepetition {
                    count_var: len_var,
                    loop_var: ivar,
                    patterns: field.get_packing(
                        &path.add_index_for_children(ivar),
                        tempnamer,
                        parent_endian,
                    ),
                };
                vec![pat]
            }),
            Field::Bits { name, items, meta } => {
                tempnamer.mark_bitfield_buf_used();

                let bitfield_path = name
                    .as_ref()
                    .map_or_else(|| path.clone(), |n| path.child(n));

                let my_endian = meta.endian.unwrap_or(parent_endian);

                split_bitfield_to_bytes(items, my_endian, meta.bitfield_align.unwrap_or_default())
                    .iter()
                    .map(|byte_items| {
                        byte_items
                            .iter()
                            .map(|chunk| {
                                let data = match &chunk.entry {
                                    BitfieldScalar::Simple {
                                        name,
                                        dtype,
                                        meta: _,
                                    } => Some(BitfieldPatternData::Field {
                                        name: bitfield_path.child(name).to_string(),
                                        ty: *dtype,
                                        ty_cast: dtype.as_unsigned(),
                                    }),
                                    BitfieldScalar::Const { value, meta: _ } => {
                                        if value.to_u64() & chunk.mask == 0 {
                                            return None;
                                        }

                                        Some(BitfieldPatternData::Const(value.to_u64()))
                                    }
                                    BitfieldScalar::Pad => {
                                        return None;
                                    }
                                };

                                data.map(|data| BitfieldPattern {
                                    data,
                                    mask: chunk.mask,
                                    rshift: chunk.rshift as i8,
                                    high_bit: chunk.bit_start as u8,
                                    count: chunk.bits as u8,
                                    partial: chunk.partial,
                                })
                            })
                            .flatten()
                            .collect::<Vec<BitfieldPattern>>()
                    })
                    .map(|pat| PackingPattern::BitfieldByte { patterns: pat })
                    .collect()
            }
        }
    }
}
