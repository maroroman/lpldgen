use crate::spec::field::{BitfieldEntry, BitfieldEntryKind, BitfieldScalar};

use crate::spec::{field::BitfieldAlign, Endian};
use ifmt::iformat;

/// Bitfield chunk (part of a byte)
#[derive(Debug)]
pub struct BitfieldChunk {
    /// Bitfield entry
    pub entry: BitfieldScalar,
    /// Mask (shifted bit sequence)
    pub mask: u64,
    // bit count
    pub bits: usize,
    // bit start
    pub bit_start: usize,
    // bit end
    pub bit_end: usize,
    /// Shift the masked field to right
    pub rshift: isize,
    /// True if split over multiple bytes/chunks
    pub partial: bool,
}

#[derive(Debug)]
struct BitfieldSplitter {
    entries: Vec<Vec<BitfieldChunk>>,
    /// Remaining unused bits in the current byte
    bit_space: usize,
    /// Mapping of variable slices to bytes
    bytechunks: Vec<BitfieldChunk>,
    /// Default endian
    endian: Endian,
    /// Field alignment
    align: BitfieldAlign,
}

impl BitfieldSplitter {
    /// Place a bit slice of a variable into one or more bytes
    fn place_subchunk(
        &mut self,
        field: &BitfieldScalar,
        msb: usize,
        lsb: usize,
        mut partial: bool,
    ) {
        let mut bitstoplace = msb + 1 - lsb;
        let mut placed = 0;
        while bitstoplace > 0 {
            let chunk = self.bit_space.min(bitstoplace);
            match self.align {
                BitfieldAlign::Right => {
                    self.bytechunks.push(BitfieldChunk {
                        entry: field.clone(),
                        mask: (((1 << chunk) - 1) << (placed + lsb)) as u64,
                        bits: chunk as usize,
                        bit_start: (lsb + placed + chunk - 1) as usize,
                        bit_end: (lsb + placed) as usize,
                        rshift: (placed + lsb) as isize - (8 - self.bit_space) as isize,
                        partial,
                    });
                }
                BitfieldAlign::Left => {
                    self.bytechunks.push(BitfieldChunk {
                        entry: field.clone(),
                        mask: (((1 << chunk) - 1) << (bitstoplace - chunk + lsb)) as u64,
                        bits: chunk as usize,
                        bit_start: (lsb + bitstoplace - 1) as usize,
                        bit_end: (lsb + bitstoplace - chunk) as usize,
                        rshift: (bitstoplace - chunk + lsb) as isize
                            - (self.bit_space as isize - chunk as isize),
                        partial,
                    });
                }
            }

            if chunk != bitstoplace {
                partial = true;
            }

            self.bit_space -= chunk;
            bitstoplace -= chunk;
            placed += chunk;

            if self.bit_space == 0 {
                self.bit_space = 8;
                self.entries.push(self.bytechunks.split_off(0));
            }
        }
    }

    /// Place a field into one or more bytes
    fn place(&mut self, field: &BitfieldScalar, bits: usize) {
        let field_endian = field.get_endian().unwrap_or(self.endian);

        match field_endian {
            Endian::Big => self.place_subchunk(field, bits - 1, 0, false),
            Endian::Little => {
                let mut remain = bits;
                let mut lsb = 0;
                let mut partial = false;
                while remain > 0 {
                    let chunk = remain.min(8);
                    self.place_subchunk(field, lsb + chunk - 1, lsb, partial);

                    if chunk != remain {
                        partial = true;
                    }

                    lsb += chunk;
                    remain -= chunk;
                }
            }
        }
    }

    fn split_field(&mut self, field: &BitfieldEntry, name_prefix: &str) {
        match &field.field {
            BitfieldEntryKind::Scalar(scalar) => {
                if name_prefix.is_empty() {
                    self.place(scalar, field.bits);
                } else {
                    let mut copy = scalar.clone();
                    if let BitfieldScalar::Simple { .. } = &copy {
                        copy =
                            copy.with_name(iformat!("{name_prefix}{scalar.get_name().unwrap()}"));
                    }

                    self.place(&copy, field.bits);
                }
            }
            BitfieldEntryKind::ScalarArray {
                count,
                field: inner,
            } => {
                for i in 0..*count {
                    // alter name to include the array indexing
                    let mut copy = inner.clone();

                    if let BitfieldScalar::Simple { .. } = &copy {
                        copy = copy
                            .with_name(iformat!("{name_prefix}{inner.get_name().unwrap()}[{i}]"));
                    }

                    self.place(&copy, field.bits / *count);
                }
            }
            BitfieldEntryKind::Struct {
                name,
                fields,
                meta: _,
            } => {
                for field in fields {
                    self.split_field(field, &iformat!("{name_prefix}{name}."));
                }
            }
            BitfieldEntryKind::StructArray {
                count,
                name,
                fields,
                meta: _,
            } => {
                for i in 0..*count {
                    for field in fields {
                        self.split_field(field, &iformat!("{name_prefix}{name}[{i}]."));
                    }
                }
            }
        }
    }
}

/// Split a bitfield to byte chunks for payload building or parsing.
///
/// Multi-byte fields' endian is resolved internally
pub fn split_bitfield_to_bytes(
    fields: &[BitfieldEntry],
    endian: Endian,
    align: BitfieldAlign,
) -> Vec<Vec<BitfieldChunk>> {
    let mut bsp = BitfieldSplitter {
        entries: vec![],
        bit_space: 8,
        bytechunks: vec![],
        endian,
        align,
    };

    for f in fields {
        bsp.split_field(f, "");
    }

    bsp.entries
}

// TODO unit tests
