use crate::codegen::utils::Indent;
use crate::spec::field::{BitfieldEntry, BitfieldEntryKind, BitfieldScalar, Field};
use crate::spec::PayloadSpec;

use crate::codegen::CodegenConfig;
use crate::modules::SpecModule;
use crate::spec::tag::MsgTag;
use ifmt::iformat;

/// Generate C declarations for the payload data structs
pub(crate) fn build_payload_structs(
    config: &CodegenConfig,
    module: &SpecModule,
    specs: &[PayloadSpec],
) -> String {
    let mut items = Vec::new();

    for spec in specs {
        let fields: String = spec
            .fields
            .iter()
            .filter_map(|f| f.c_definition())
            .collect::<Vec<String>>()
            .join("\n");

        let named_constants = spec.get_named_constants();

        let stru_name = module.substitute_name(&config.tpl_payload_struct, &spec.name);

        let comment = match &spec.descr {
            None => {
                format!("/** Payload \"{}\" */", spec.name)
            }
            Some(descr) => {
                format!(
                    "/**\n * Payload \"{}\"\n *\n * {}\n */",
                    spec.name,
                    descr.replace("\n", "\n * ")
                )
            }
        }
        .indent_inner(4);

        items.push(
            format!(
                r#"
                {comment}
                struct {stru_name} {{
                    uint16_t {pld_id}; //!< Payload ID
                    {fields}
                }};
                "#,
                pld_id = config.pld_id_field,
                comment = comment,
                stru_name = stru_name,
                fields = if fields.is_empty() {
                    "/* empty */"
                } else {
                    &fields
                }
                .indent_inner(5),
            )
            .unindent(),
        );

        if !named_constants.is_empty() {
            let f_const_name = module.substitute_name(&config.tpl_named_const, &spec.name);
            items.push("/* Associated constant values */".to_string());
            for (name, value) in named_constants {
                let name = name.replace(|c: char| !c.is_ascii_alphanumeric() || c == '_', "_");

                let defname = f_const_name
                    .replace("%Const", &name)
                    .replace("%CONST", &name.to_ascii_uppercase())
                    .replace("%const", &name.to_ascii_lowercase());

                let defvalue = value.to_c_literal();

                items.push(iformat!("#define {defname} {defvalue}"));
            }
        }

        let f_binary_size = module.substitute_name(&config.tpl_binary_size_const, &spec.name);
        let f_struct_c_size = module.substitute_name(&config.tpl_struct_c_size_const, &spec.name);

        if spec.is_growable() {
            let tail = spec.fields.last().unwrap();
            let tailname = if let Field::TailArray { field } = tail {
                field.get_name().expect("Tail array field must have name!")
            } else {
                structural_error!(
                    "Error getting tail array field - not last in the struct? (in \"{}\")",
                    spec.name
                );
            };

            let f_tail_c_size =
                module.substitute_name(&config.tpl_struct_tail_elem_c_size_const, &spec.name);

            items.push(iformat!(
                "/** sizeof({spec.name}) with a tail of given size */\n\
                #define {f_tail_c_size} sizeof(((struct {stru_name}*)0)->{tailname}[0])"
            ));

            items.push(iformat!(
                "/** sizeof({spec.name}) with a tail of given size */\n\
                #define {f_struct_c_size}(_tail_count) (sizeof(struct {stru_name}) + {f_tail_c_size} * (_tail_count))"
            ));

            let binary_size = spec.byte_length();
            let (_, tail_bytes) = spec
                .tail_array_info()
                .expect("tail array info for a growable spec!");

            let f_tail_binary_size =
                module.substitute_name(&config.tpl_binary_tail_elem_size_const, &spec.name);

            items.push(iformat!(
                "/** Byte size of {stru_name} in binary form (for a given tail length) */\n\
                #define {f_tail_binary_size} {tail_bytes}\n\
                \n\
                /** Byte size of {stru_name} in binary form (for a given tail length) */\n\
                #define {f_binary_size}(_tail_elems) ({binary_size} + (_tail_elems) * {f_tail_binary_size})"
            ));
        } else {
            let binary_size = spec.byte_length();

            items.push(iformat!(
                "/** sizeof({spec.name}) */\n\
                #define {f_struct_c_size} (sizeof(struct {stru_name}))"
            ));

            items.push(iformat!(
                "/** Byte size of {stru_name} in binary form */\n\
                #define {f_binary_size} {binary_size}"
            ));
        }

        match spec.tag {
            MsgTag::Template => {
                panic!("Template uses should be resolved by now");
            }
            MsgTag::From(a, p) => {
                let f_addr = module.substitute_name(&config.tpl_struct_src_addr_const, &spec.name);
                let f_port = module.substitute_name(&config.tpl_struct_src_port_const, &spec.name);

                items.push(iformat!(
                    "/** Payload {stru_name} CSP source address */\n\
                    #define {f_addr} {a}\n\
                    \n\
                    /** Payload {stru_name} CSP source port */\n\
                    #define {f_port} {p}"
                ));
            }
            MsgTag::To(a, p) => {
                let f_addr = module.substitute_name(&config.tpl_struct_dst_addr_const, &spec.name);
                let f_port = module.substitute_name(&config.tpl_struct_dst_port_const, &spec.name);

                items.push(iformat!(
                    "/** Payload {stru_name} CSP destination address */\n\
                    #define {f_addr} {a}\n\
                    \n\
                    /** Payload {stru_name} CSP destination port */\n\
                    #define {f_port} {p}"
                ));
            }
        }
    }

    items.join("\n\n")
}

/// C generation extensions for Field
trait CFieldType {
    /// Build the structure member declaration for the field, or None if it is a padding or constant
    fn c_definition(&self) -> Option<String>;

    /// Get C type for the field; in case of bitfield or struct, return an anonymous struct
    fn c_type(&self) -> String;
}

impl CFieldType for Field {
    /// Returns the complete struct member, including a trailing semicolon.
    fn c_definition(&self) -> Option<String> {
        let code = match self {
            Field::Use { .. } => {
                panic!("Template uses should be resolved by now");
            }
            Field::Simple { name, meta, .. } | Field::Struct { name, meta, .. } => {
                let ty = self.c_type();

                let comment = meta.to_field_comment().unwrap_or_default();

                iformat!("{comment}{ty} {name};")
            }
            Field::Const { .. } | Field::Pad(_) => return None,
            Field::Bits {
                name,
                items: _,
                meta,
            } => {
                let ty = self.c_type();

                let comment = meta.to_field_comment().unwrap_or_default();

                if let Some(name) = name {
                    iformat!("{comment}{ty} {name};")
                } else {
                    // in this case the struct is not even rendered
                    iformat!("{comment}{ty}")
                }
            }
            Field::Array { count, field } => {
                let ty = field.c_type();
                let name = field.get_name().expect("field in array must have name");

                let comment = match field.get_meta() {
                    None => "".to_string(),
                    Some(meta) => meta.to_field_comment().unwrap_or_default(),
                };

                iformat!("{comment}{ty} {name}[{count}];")
            }
            Field::TailArray { field } => {
                let ty = field.c_type();
                let name = field
                    .get_name()
                    .expect("field in tail array must have name");

                let comment = match field.get_meta() {
                    None => "".to_string(),
                    Some(meta) => meta.to_field_comment().unwrap_or_default(),
                };

                // we prefix the tail field with a synthetic length field.
                // This is needed to pass this extra information around when only the struct
                // is used as an argument.
                iformat!(
                    "size_t {name}_len; //!< Length of the \"{name}\"\n\
                    {comment}{ty} {name}[];"
                )
            }
        };

        Some(code)
    }

    /// Returns the type definition, e.g. uint8_t or struct { ... }, without name and array brackets.
    /// Inner fields inside struct or bitfield are rendered fully.
    fn c_type(&self) -> String {
        match self {
            Field::Use { .. } => {
                panic!("Template uses should be resolved by now");
            }
            // Simple field, like uint8_t
            Field::Simple { dtype, meta, .. } => {
                if let Some(ty) = meta.repr {
                    ty.c_name().into()
                } else {
                    dtype.c_name().into()
                }
            }
            // Structural elements that hold no information
            Field::Const { .. } | Field::Pad(_) => {
                // should not be rendered to the struct, hence unreachable to catch bugs
                unreachable!()
            }
            // Bitfield
            Field::Bits { name, items, .. } => {
                let inner = items
                    .iter()
                    .filter_map(bitfield_def)
                    .collect::<Vec<String>>()
                    .join("\n");

                if name.is_some() {
                    format!("struct {{\n{}\n}}", inner.indent(1))
                } else {
                    inner
                }
            }
            // A struct containing sub-fields, possibly even sub-structs.
            Field::Struct { fields, .. } => {
                let inner = fields
                    .iter()
                    .filter_map(Field::c_definition)
                    .collect::<Vec<String>>()
                    .join("\n");

                format!("struct {{\n{}\n}}", inner.indent(1))
            }
            // Array; the brackets with element count are added in c_definition()
            Field::Array { field, .. } | Field::TailArray { field } => field.c_type(),
        }
    }
}

/// Get structure member(s) representing a bitfield entry
fn bitfield_def(f: &BitfieldEntry) -> Option<String> {
    match &f.field {
        // Simple field.
        // Bitfield can also contain const and pad, but these are not rendered to the struct definition.
        BitfieldEntryKind::Scalar(BitfieldScalar::Simple { name, dtype, meta }) => {
            if meta.has_conversions() {
                structural_error!("Conversions may not be used inside bitfields ({})", name);
            }

            let ty = dtype.c_name();

            let comment = meta.to_field_comment().unwrap_or_default();

            Some(iformat!("{comment}{ty} {name};"))
        }
        // Array of simple fields.
        BitfieldEntryKind::ScalarArray {
            count: num,
            field: BitfieldScalar::Simple { name, dtype, meta },
        } => {
            if meta.has_conversions() {
                structural_error!("Conversions may not be used inside bitfields ({})", name);
            }

            // inner can be Simple, Const or Pad. Only Simple holds information
            let ty = dtype.c_name();

            let comment = meta.to_field_comment().unwrap_or_default();

            Some(iformat!("{comment}{ty} {name}[{num}];"))
        }
        BitfieldEntryKind::ScalarArray {
            field: BitfieldScalar::Pad,
            ..
        }
        | BitfieldEntryKind::ScalarArray {
            field: BitfieldScalar::Const { .. },
            ..
        } => {
            structural_error!("Bad field in bitfield scalar array: {:?}", f.field);
        }

        BitfieldEntryKind::Struct { name, fields, meta } => {
            let inner = fields
                .iter()
                .filter_map(bitfield_def)
                .collect::<Vec<String>>()
                .join("\n")
                .indent_inner(6);

            let comment = meta.to_field_comment().unwrap_or_default();

            Some(
                iformat!(
                    r#"
                    {comment}struct {{
                        {inner}
                    }} {name};
                    "#
                )
                .unindent()
                .trim()
                .to_string(),
            )
        }

        BitfieldEntryKind::StructArray {
            name,
            count,
            fields,
            meta,
        } => {
            let inner = fields
                .iter()
                .filter_map(bitfield_def)
                .collect::<Vec<String>>()
                .join("\n")
                .indent_inner(6);

            let comment = meta.to_field_comment().unwrap_or_default();

            Some(
                iformat!(
                    r#"
                    {comment}struct {{
                        {inner}
                    }} {name}[{count}];
                    "#
                )
                .unindent()
                .trim()
                .to_string(),
            )
        }

        BitfieldEntryKind::Scalar(BitfieldScalar::Const { .. }) => None,

        BitfieldEntryKind::Scalar(BitfieldScalar::Pad) => None,
    }
}
