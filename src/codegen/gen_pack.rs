use std::borrow::Cow;

use ifmt::iformat;

use super::packing_layout::{BitfieldPattern, BitfieldPatternData, FieldPacking, PackingPattern};
use super::utils::{Indent, PushString, ShiftLeftRight, StructPath, TempNamer};
use crate::codegen::packing_layout::Transform;
use crate::spec::field::Type;
use crate::spec::PayloadSpec;

use crate::codegen::CodegenConfig;
use crate::modules::SpecModule;
use crate::parser::expr::ast::FloatPrecision;
use std::cmp::Ordering;

/// Build the builder function prototypes (for the header)
pub(crate) fn build_builder_protos(
    config: &CodegenConfig,
    module: &SpecModule,
    specs: &[PayloadSpec],
) -> String {
    // TODO group by static
    let mut items = Vec::new();

    for spec in specs {
        let name = &spec.name;
        let func = module.substitute_name(&config.tpl_build_fn, &spec.name);
        let func_s = module.substitute_name(&config.tpl_build_static_fn, &spec.name);
        let stru = module.substitute_name(&config.tpl_payload_struct, &spec.name);

        let extra = if spec.has_variables() {
            ""
        } else {
            " Data may be NULL."
        };

        items.push(iformat!(r##"
            #if PLD_HAVE_ALLOC
            /** @brief Encode payload struct "{name}" to its binary form.{extra} */
            pld_error_t {func}(const struct {stru} *data, pld_alloc_fn alloc, uint8_t **output, size_t *len);
            #endif /* PLD_HAVE_ALLOC */

            /** @brief Encode payload struct "{name}" to its binary form.{extra} (static version) */
            pld_error_t {func_s}(const struct {stru} *data, uint8_t *output, size_t capacity);
            "##
        ).unindent());
    }

    items.join("\n\n")
}

/// Build functions that construct the binary payload from a data struct
pub(crate) fn build_builder_funcs(
    config: &CodegenConfig,
    module: &SpecModule,
    specs: &[PayloadSpec],
) -> String {
    // TODO group by static
    let mut items = Vec::new();

    for spec in specs {
        let (body, body_s) = gen_payload_builder_code(config, module, spec);

        let body = body.indent_inner(5);
        let body_s = body_s.indent_inner(5);

        let func = module.substitute_name(&config.tpl_build_fn, &spec.name);
        let func_s = module.substitute_name(&config.tpl_build_static_fn, &spec.name);
        let stru = module.substitute_name(&config.tpl_payload_struct, &spec.name);
        items.push(
            iformat!(
                r##"
                #if PLD_HAVE_ALLOC
                pld_error_t {func}(const struct {stru} * const data, pld_alloc_fn alloc, uint8_t **output, size_t *len) {{
                    pld_log_trace("{func}");
                    {body}
                }}
                #endif /* PLD_HAVE_ALLOC */

                pld_error_t {func_s}(const struct {stru} * const data, uint8_t *output, size_t capacity) {{
                    pld_log_trace("{func_s}");
                    {body_s}
                }}
                "##
            )
            .unindent(),
        );
    }

    items.join("\n\n")
}

/// Build a payload builder function's body
fn gen_payload_builder_code(
    config: &CodegenConfig,
    module: &SpecModule,
    spec: &PayloadSpec,
) -> (String, String) {
    let mut buf = String::new();

    let pld_len = spec.byte_length();
    let f_pld_id = &config.pld_id_field;
    let pld_id = module.substitute_name(&config.tpl_enum_item, &spec.name);
    let f_binary_size = module.substitute_name(&config.tpl_binary_size_const, &spec.name);

    let id_checks = iformat!(r##"
        #if PLD_BUILD_FUNCS_CHECK_ID == 1
            if (data && data->{f_pld_id} != {pld_id}) {{
                pld_log_error("payload ID %d does not match expected %d", data->{f_pld_id}, {pld_id});
                return PLD_ERR_ID_MISMATCH;
            }}
        #elif PLD_BUILD_FUNCS_CHECK_ID == 2
            if (data && data->{f_pld_id} != PAYLOAD_ID_NONE && data->{f_pld_id} != {pld_id}) {{
                pld_log_error("payload ID %d set and does not match expected %d", data->{f_pld_id}, {pld_id});
                return PLD_ERR_ID_MISMATCH;
            }}
        #endif
    "##).unindent();

    if spec.is_growable() {
        // The only way for it to be growable, currently, is to have the last field a TailArray
        let (tail_name, _) = spec.tail_array_info().expect("tail info");

        buf.push_string(iformat!(
            "const size_t byte_size = {f_binary_size}(data->{tail_name}_len);\n"
        ));
    } else {
        if pld_len == 0 {
            let checks = id_checks.indent_inner(5);
            // NULL data is allowed here, but if given, the ID must match.
            return (
                // Full
                iformat!(
                    r##"
                    {checks}

                    if (!output) {{
                        pld_log_error("output is NULL");
                        return PLD_ERR_BAD_ARGS;
                    }}

                    if (!len) {{
                        pld_log_error("len is NULL");
                        return PLD_ERR_BAD_ARGS;
                    }}

                    uint8_t *pld = alloc(1);
                    if (NULL == pld) {{
                        pld_log_error("payload buffer alloc failed");
                        return PLD_ERR_ALLOC;
                    }}

                    if (output) *output = pld;
                    if (len) *len = 0;
                    return PLD_OK;
                "##
                )
                .unindent(),
                // Raw
                iformat!(
                    r##"
                    {checks}
                    (void)output;
                    (void)capacity;
                    return PLD_OK;
                "##
                )
                .unindent(),
            );
        }

        buf.push_string(iformat!("const size_t byte_size = {f_binary_size};\n"))
    }

    let mut buf_s = buf.clone(); // both need byte_size

    let checks = id_checks.indent_inner(2);

    if spec.has_variables() {
        let s = iformat!(
            r#"
            if (!data) {{
                pld_log_error("data is NULL");
                return PLD_ERR_BAD_ARGS;
            }}
        "#
        )
        .unindent()
            + "\n\n";

        buf.push_string(s.clone());
        buf_s.push_string(s);
    }

    buf.push_string(
        iformat!(
            r#"
        if (!output) {{
            pld_log_error("output is NULL");
            return PLD_ERR_BAD_ARGS;
        }}

        if (!len) {{
            pld_log_error("len is NULL");
            return PLD_ERR_BAD_ARGS;
        }}

        {checks}

        uint8_t *pld = alloc(byte_size);
        if (NULL == pld) {{
            pld_log_error("payload buffer alloc failed");
            return PLD_ERR_ALLOC;
        }}

        *output = pld;
        *len = byte_size;
        "#
        )
        .unindent()
            + "\n\n",
    );

    buf_s.push_string(iformat!(
        r#"
        if (!output) {{
            pld_log_error("output is NULL");
            return PLD_ERR_BAD_ARGS;
        }}

        {checks}

        if (capacity < byte_size) {{
            pld_log_error("insufficient buffer capacity (%d, need %d)", (int)capacity, (int)byte_size);
            return PLD_ERR_LENGTH;
        }}
        "#)
        .unindent()
            + "\n\n",
    );

    let mut tempnamer = TempNamer::new();
    for field in &spec.fields {
        let path = StructPath::new("data", true);
        buf_s.push_string(
            field
                .get_packing(&path, &mut tempnamer, spec.endian)
                .build_packing_code("output", &mut tempnamer)
                + "\n\n",
        );
    }

    let func_s = module.substitute_name(&config.tpl_build_static_fn, &spec.name);
    buf.push_str(&iformat!("return {func_s}(data, pld, byte_size);"));
    buf_s.push_str("return PLD_OK;");

    (
        // Full
        buf.trim().to_string(),
        // Raw
        (tempnamer.c_declarations() + &buf_s).trim().to_string(),
    )
}

pub trait FieldPackingCodeGen {
    fn build_packing_code(&self, bytes_var: &str, tempnamer: &mut TempNamer) -> String;
}

impl FieldPackingCodeGen for PackingPattern {
    fn build_packing_code(&self, bytes_var: &str, tempnamer: &mut TempNamer) -> String {
        let transmute_to_unsigned = |name: &str, src: Type| match src {
            Type::U8 | Type::U16 | Type::U24 | Type::U32 | Type::U64 => {
                iformat!("({src.c_name()})({name})")
            }
            Type::I8 => iformat!("((x8_t){{ .i=(int8_t)({name}) }}).u"),
            Type::I16 => iformat!("((x16_t){{ .i=({name}) }}).u"),
            Type::I24 | Type::I32 => iformat!("((x32_t){{ .i=({name}) }}).u"),
            Type::I64 => iformat!("((x64_t){{ .i=({name}) }}).u"),
            Type::F32 => iformat!("((x32_t){{ .f=({name}) }}).u"),
            Type::F64 => iformat!("((x64_t){{ .f=({name}) }}).u"),
            Type::Bool => iformat!("((uint8_t)({name}))"),
        };

        let mut render_transform = |varname: &str, transform: &Transform, binary_ty: Type| {
            let tmpvar = tempnamer.tmp_bytes(binary_ty.byte_size());

            let floats = if transform.ty_repr == Type::F64 {
                FloatPrecision::Double
            } else {
                FloatPrecision::Float
            };

            let round = ![Type::F64, Type::F32].contains(&binary_ty);

            let fl_c = if (binary_ty.is_floating() || transform.ty_repr.is_floating())
                || !transform.is_trivial()
            {
                let mut convert = transform.convert.clone();
                convert.substitute_var("x", varname);
                convert.render_c(floats, round)
            } else {
                varname.to_string()
            };

            let assign = if binary_ty.is_floating() {
                let mut src = transform.ty_repr;
                // special cases
                let c = if transform.ty_repr == Type::F64 && binary_ty == Type::F32 {
                    src = Type::F32;
                    let fl_c2 = iformat!("(float) {fl_c}");
                    transmute_to_unsigned(&fl_c2, src)
                } else if transform.ty_repr == Type::F32 && binary_ty == Type::F64 {
                    src = Type::F64;
                    let fl_c2 = iformat!("(double) {fl_c}");
                    transmute_to_unsigned(&fl_c2, src)
                } else {
                    transmute_to_unsigned(&fl_c, src)
                };
                iformat!("{tmpvar} = {c};")
            } else {
                iformat!("{tmpvar} = ({binary_ty.c_name()}) {fl_c};")
            };

            (tmpvar, assign)
        };

        match self {
            PackingPattern::Byte {
                name,
                ty,
                transform,
            } => {
                if let Some(tr) = transform {
                    let (tmpvar, c_calc) = render_transform(name.as_str(), tr, *ty);
                    iformat!("{c_calc}\n*{bytes_var}++ = {tmpvar};")
                } else {
                    let c = transmute_to_unsigned(name, *ty);
                    iformat!("*{bytes_var}++ = {c};")
                }
            }

            PackingPattern::ByteConst(v) => {
                if *v >= 32 && *v < 127 {
                    let as_char = *v as char;
                    iformat!("*{bytes_var}++ = {v:#04x}; /* '{as_char}' */")
                } else {
                    iformat!("*{bytes_var}++ = {v:#04x};")
                }
            }

            PackingPattern::MultiByte {
                name,
                ty,
                bytes,
                transform,
            } => {
                let mut buf = String::new();
                let name = if let Some(tr) = transform {
                    let (tmpvar, c_calc) = render_transform(name.as_str(), tr, *ty);
                    buf.push_string(c_calc);
                    buf.push('\n');

                    tmpvar.to_string()
                } else {
                    let tmpvar = tempnamer.tmp_bytes(ty.byte_size());
                    let c = transmute_to_unsigned(name, *ty);
                    buf.push_string(iformat!("{tmpvar} = {c};\n"));
                    tmpvar.to_string()
                };

                buf + &bytes
                    .iter()
                    .map(|pos| {
                        if *pos == 0 {
                            iformat!("*{bytes_var}++ = (uint8_t) ({name} & 0xff);")
                        } else {
                            iformat!("*{bytes_var}++ = (uint8_t) (({name} >> {pos*8}) & 0xff);")
                        }
                    })
                    .collect::<Vec<_>>()
                    .join("\n")
            }

            PackingPattern::FixedRepetition {
                count,
                loop_var,
                patterns,
            } => {
                let inner = patterns
                    .build_packing_code(bytes_var, tempnamer)
                    .indent_inner(6);

                iformat!(
                    r#"
                    for ({loop_var} = 0; {loop_var} < {count}; {loop_var}++) {{
                        {inner}
                    }}
                "#
                )
                .unindent()
            }
            PackingPattern::DynamicRepetition {
                count_var,
                loop_var,
                patterns,
            } => {
                let inner = patterns
                    .build_packing_code(bytes_var, tempnamer)
                    .indent_inner(6);

                iformat!(
                    r#"
                    for ({loop_var} = 0; {loop_var} < {count_var}; {loop_var}++) {{
                        {inner}
                    }}
                "#
                )
                .unindent()
            }
            PackingPattern::BitfieldByte { patterns } => {
                patterns.build_packing_code(bytes_var, tempnamer)
            }
        }
    }
}

impl FieldPackingCodeGen for Vec<PackingPattern> {
    fn build_packing_code(&self, bytes_var: &str, tempnamer: &mut TempNamer) -> String {
        self.iter()
            .map(|p| p.build_packing_code(bytes_var, tempnamer))
            .collect::<Vec<_>>()
            .join("\n")
    }
}

impl FieldPackingCodeGen for Vec<BitfieldPattern> {
    fn build_packing_code(&self, bytes_var: &str, tempnamer: &mut TempNamer) -> String {
        if self.is_empty() {
            return iformat!("*{bytes_var}++ = 0x00;");
        }

        let mut lines = String::new();
        let b_var = tempnamer.bitfield_buf_name();
        lines.push_string(iformat!("{b_var} = 0;\n"));

        for bpat in self {
            lines.push_string(bpat.build_packing_code(b_var) + "\n");
        }
        lines.push_string(iformat!("*{bytes_var}++ = {b_var};\n"));

        lines.trim().to_string()
    }
}

/// Build packing code for a bitfield
trait BitfieldPackingCodeGen {
    fn build_packing_code(&self, byte_var: &str) -> String;
}

impl BitfieldPackingCodeGen for BitfieldPattern {
    /// Pack this bitfield byte chunk into a byte variable `byte_var`
    fn build_packing_code(&self, byte_var: &str) -> String {
        let mask = self.mask;
        let rshift = self.rshift;

        match &self.data {
            BitfieldPatternData::Field { name, ty, ty_cast } => {
                let name_cast = if ty == ty_cast {
                    Cow::Borrowed(name)
                } else {
                    Cow::Owned(iformat!("(({ty_cast.c_name()}) {name})"))
                };

                match rshift.cmp(&0) {
                    Ordering::Less => {
                        // < 0
                        iformat!(
                            "{byte_var} |= (uint8_t) (({name_cast} & {mask:#04x}) << {-rshift});"
                        )
                    }
                    Ordering::Equal => {
                        // == 0
                        iformat!("{byte_var} |= (uint8_t) ({name_cast} & {mask:#04x});")
                    }
                    Ordering::Greater => {
                        // > 0
                        iformat!(
                            "{byte_var} |= (uint8_t) (({name_cast} & {mask:#04x}) >> {rshift});"
                        )
                    }
                }
            }
            BitfieldPatternData::Const(num) => {
                let masked = num & mask;

                let piece = (masked as u64).shift(rshift);

                iformat!("{byte_var} |= {piece:#04x};")
            }
        }
    }
}
