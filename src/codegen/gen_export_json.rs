use super::utils::bitfield_to_field;
use crate::codegen::utils::{AddSlashes, Indent, NumberedTempNamer, StructPath, TempNamer};
use crate::spec::field::{Field, FieldMeta, Type};
use crate::spec::PayloadSpec;

use crate::codegen::CodegenConfig;
use crate::modules::SpecModule;
use ifmt::iformat;
use std::borrow::Cow;
use lazy_static::lazy_static;
use regex::Regex;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
enum Style {
    Dataonly,
    Annotated,
}

/// Build the export function prototypes (for the header)
pub(crate) fn build_export_protos(
    config: &CodegenConfig,
    module: &SpecModule,
    specs: &[PayloadSpec],
) -> String {
    // TODO group by static
    let mut items = Vec::new();

    for spec in specs {
        let name = &spec.name;
        let structname = module.substitute_name(&config.tpl_payload_struct, &spec.name);

        let func = module.substitute_name(&config.tpl_export_fn, &spec.name);

        items.push(
            format!(r##"
                /** @brief Export payload struct "{name}" to JSON (without look-up) */
                pld_error_t {func}(const struct {structname} *data, enum pld_export_json_style style, cJSON** output);
                "##,
                    name = name,
                    structname = structname,
                    func = func
            )
                .unindent()
        );
    }

    items.join("\n\n")
}

pub(crate) fn build_export_funcs(
    config: &CodegenConfig,
    module: &SpecModule,
    specs: &[PayloadSpec],
) -> String {
    let mut items = Vec::new();

    for spec in specs {
        let cjnamer = NumberedTempNamer::new("item");
        let mut tempnamer = TempNamer::new();

        let dataonly = gen_export(config, &cjnamer, &mut tempnamer, spec, Style::Dataonly);
        let annotated = gen_export(config, &cjnamer, &mut tempnamer, spec, Style::Annotated);

        let tmpinits = cjnamer
            .get_names()
            .iter()
            .map(|n| format!("cJSON *{} = NULL;", n))
            .collect::<Vec<String>>()
            .join("\n");

        let tninit = tempnamer.c_declarations();

        let f_pld_id = &config.pld_id_field;
        let pld_id = module.substitute_name(&config.tpl_enum_item, &spec.name);

        let structname = module.substitute_name(&config.tpl_payload_struct, &spec.name);
        let func = module.substitute_name(&config.tpl_inner_export_fn, &spec.name);

        items.push(
            format!(r#"
                static pld_error_t {func}(const struct {structname} *data, enum pld_export_json_style style, cJSON* output) {{
                    pld_log_trace("{func}");

                    if (!data) {{
                        pld_log_error("data is NULL");
                        return PLD_ERR_BAD_ARGS;
                    }}

                    if (!output) {{
                        pld_log_error("output is NULL");
                        return PLD_ERR_BAD_ARGS;
                    }}

                    #if PLD_EXPORT_FUNCS_CHECK_ID
                        if (data->{f_pld_id} != {pld_id}) {{
                            pld_log_error("payload ID %d does not match expected %d", data->{f_pld_id}, {pld_id});
                            return PLD_ERR_ID_MISMATCH;
                        }}
                    #endif

                    {tmpinits}
                    {tninit}

                    // suppress unused warnings
                    (void)data;
                    (void)output;

                    switch (style) {{
                        case EXPORT_JSON_DATAONLY:
                            pld_log_trace("export as data-only");
                            {export_dataonly}
                            break;
                        case EXPORT_JSON_ANNOTATED:
                            pld_log_trace("export annotated");
                            {export_annotated}
                            break;
                        default:
                            pld_log_error("bad export style");
                            return PLD_ERR_NOT_IMPLEMENTED;
                    }}
                    return PLD_OK;
                cjfail:
                    return PLD_ERR_ALLOC; /* will be deallocated by caller */
                }}
                "#,
                    f_pld_id = f_pld_id,
                    pld_id = pld_id,
                    tmpinits = tmpinits.indent_inner(5),
                    tninit = tninit.indent_inner(5),
                    structname = structname,
                    func = func,
                    export_dataonly = dataonly.indent_inner(7),
                    export_annotated = annotated.indent_inner(7),
            )
            .unindent()
        );

        let func = module.substitute_name(&config.tpl_export_fn, &spec.name);
        let spec_ptr = module.substitute_name(&config.tpl_spec_pointer, &spec.name);

        items.push(
            format!(r#"
                pld_error_t {func}(const struct {structname} *data, enum pld_export_json_style style, cJSON** output) {{
                    pld_log_trace("{func}");
                    return pld_to_json_by_spec({spec_ptr}, data, style, output);
                }}
                "#,
                    spec_ptr = spec_ptr,
                    structname = structname,
                    func = func
            )
            .unindent()
        );
    }

    items.join("\n\n")
}

fn gen_export(
    config: &CodegenConfig,
    cjnamer: &NumberedTempNamer,
    tempnamer: &mut TempNamer,
    spec: &PayloadSpec,
    style: Style,
) -> String {
    let path = StructPath::new("data", true);

    let mut lines = vec![];

    lines.extend(
        spec.fields
            .iter()
            .map(|f| {
                export_field(
                    config, "output", false, // not array
                    &path, cjnamer, tempnamer, f, style,
                )
            })
            .flatten(),
    );

    lines.join("\n")
}

/// Export field in data-only style
#[allow(clippy::too_many_arguments)] // todo?
fn export_field(
    config: &CodegenConfig,
    parent_var: &str,
    parent_is_array: bool,
    path: &StructPath,
    cjnamer: &NumberedTempNamer,
    tempnamer: &mut TempNamer,
    field: &Field,
    style: Style,
) -> Vec<String> {
    let mut lines: Vec<String> = vec![];

    // Build the "descr" and "type" fields in annotated variant
    #[must_use]
    let annotated_base_build = |cjitem: &str, tyname: &str, meta: &FieldMeta| {
        let mut lines = vec![];

        if let Some(d) = &meta.descr {
            let cjtmp = cjnamer.claim();
            let q_descr = d.add_slashes_and_quotes();
            lines.push(iformat!(
                "CJSON_TRY({cjtmp} = cJSON_CreateStringReference({q_descr}));"
            ));
            lines.push(iformat!(
                "cJSON_AddItemToObjectCS({cjitem}, \"descr\", {cjtmp});"
            ));
        }

        if let Some(u) = &meta.unit {
            let cjtmp = cjnamer.claim();
            let q_unit = u.add_slashes_and_quotes();
            lines.push(iformat!(
                "CJSON_TRY({cjtmp} = cJSON_CreateStringReference({q_unit}));"
            ));
            lines.push(iformat!(
                "cJSON_AddItemToObjectCS({cjitem}, \"unit\", {cjtmp});"
            ));
        }

        // type is always present
        {
            let cjtmp = cjnamer.claim();
            let q_tyname = tyname.add_slashes_and_quotes();
            lines.push(iformat!(
                "CJSON_TRY({cjtmp} = cJSON_CreateStringReference({q_tyname}));"
            ));
            lines.push(iformat!(
                "cJSON_AddItemToObjectCS({cjitem}, \"type\", {cjtmp});"
            ));
        }

        lines
    };

    // Array match branch (shared by simple and tail array)
    // - loop_max : max loop variable value
    // - count_value : count variable name or literal
    #[must_use]
    let mut array_build = |loop_max: usize, count_value: &str, arfield: &Field| {
        let mut lines = vec![];

        // copy of style that can be overwritten (used for scalar arrays)
        let mut local_style = style;

        let cjitem = cjnamer.claim();
        let q_name = arfield.get_name().unwrap().add_slashes_and_quotes();

        let mut cjval = None;
        let array_var_name;

        match local_style {
            Style::Dataonly => {
                lines.push(iformat!("CJSON_TRY({cjitem} = cJSON_CreateArray());"));
                if parent_is_array {
                    lines.push(iformat!("cJSON_AddItemToArray({parent_var}, {cjitem});"));
                } else {
                    lines.push(iformat!(
                        "cJSON_AddItemToObjectCS({parent_var}, {q_name}, {cjitem});"
                    ));
                }

                array_var_name = cjitem.name();
            }
            Style::Annotated => {
                lines.push(iformat!("CJSON_TRY({cjitem} = cJSON_CreateObject());"));
                if parent_is_array {
                    lines.push(iformat!("cJSON_AddItemToArray({parent_var}, {cjitem});"));
                } else {
                    lines.push(iformat!(
                        "cJSON_AddItemToObjectCS({parent_var}, {q_name}, {cjitem});"
                    ));
                }

                let dty: Cow<'static, str> = match arfield {
                    Field::Use { .. } => {
                        panic!("Template uses should be resolved by now");
                    }
                    Field::Simple { dtype, .. } => {
                        // the insides are not annotated
                        local_style = Style::Dataonly;

                        format!("{}[]", dtype.short_name()).into()
                    }

                    Field::Bits { .. } | Field::Struct { .. } => "struct[]".into(),
                    Field::Const { .. } | Field::Pad(_) => {
                        unimplemented!(); // const/pad arrays are not implemented in the parser
                    }
                    Field::Array { .. } | Field::TailArray { .. } => {
                        unimplemented!(); // multi-dimensional arrays are not implemented in the parser
                    }
                };

                lines.extend(annotated_base_build(
                    &cjitem.to_string(),
                    &dty,
                    field.get_meta().unwrap_or(&FieldMeta::default()),
                ));

                let m_cjval = cjnamer.claim();
                lines.push(iformat!("CJSON_TRY({m_cjval} = cJSON_CreateArray());"));
                lines.push(iformat!(
                    "cJSON_AddItemToObjectCS({cjitem}, \"value\", {m_cjval});"
                ));

                array_var_name = m_cjval.name();

                cjval.replace(m_cjval);
            }
        }

        lines.push(tempnamer.descend(|tempnamer: &mut TempNamer| {
            let ivar = tempnamer.loopvar(loop_max);
            let inner = export_field(
                config,
                &array_var_name,
                true,
                &path.add_index_for_children(ivar),
                cjnamer,
                tempnamer,
                arfield,
                local_style,
            )
            .join("\n")
            .indent_inner(6);

            iformat!(
                r#"
                    for ({ivar} = 0; {ivar} < {count_value}; {ivar}++) {{
                        {inner}
                    }}"#
            )
            .unindent()
        }));

        drop(cjitem);
        drop(cjval);
        lines
    };

    match field {
        Field::Use { .. } => {
            panic!("Template uses should be resolved by now");
        }
        Field::Simple { name, dtype, meta } => {
            let value = path.child(name);
            let q_name = name.add_slashes_and_quotes();
            let e_name = &q_name[1..(q_name.len() - 1)]; // what's inside the quotes

            let cjtype = match dtype {
                Type::Bool => "Bool",
                _ => "Number",
            };

            let cjval = cjnamer.claim();
            let ln_create_value = if let Some(fmt) = &meta.json_sprintf {
                let tmps = tempnamer.tmp_str();
                let tmpi = tempnamer.tmp_int();
                let qfmt0 = fmt.add_slashes_and_quotes();
                let qfmt = apply_inttypes_to_quoted_printf_format(&qfmt0, dtype.byte_size());

                // asprintf malloc's the string.
                // cJSON_CreateString uses strdup internally, so we must free the string in both cases
                iformat!(
                    r#"
                    {tmpi} = asprintf(&{tmps}, {qfmt}, {value});
                    if (tmpi < 0) {{
                        pld_log_error("Error formatting {e_name}");
                        goto cjfail;
                    }}
                    CJSON_TRY_F({cjval} = cJSON_CreateString({tmps}), {tmps});
                    free({tmps});
                "#
                )
                .unindent()
            } else {
                iformat!("CJSON_TRY({cjval} = cJSON_Create{cjtype}({value}));")
            };

            match style {
                Style::Dataonly => {
                    /*
                        "name": 15,
                    */
                    lines.push(ln_create_value);
                    if parent_is_array {
                        lines.push(iformat!("cJSON_AddItemToArray({parent_var}, {cjval});"));
                    } else {
                        lines.push(iformat!(
                            "cJSON_AddItemToObjectCS({parent_var}, {q_name}, {cjval});"
                        ));
                    }
                }
                Style::Annotated => {
                    /*
                        "foo": {
                            "descr": "Descr",
                            "type": "u16",
                            "value": 15
                        },
                    */

                    let cjitem = cjnamer.claim();
                    lines.push(iformat!("CJSON_TRY({cjitem} = cJSON_CreateObject());"));
                    if parent_is_array {
                        lines.push(iformat!("cJSON_AddItemToArray({parent_var}, {cjitem});"));
                    } else {
                        lines.push(iformat!(
                            "cJSON_AddItemToObjectCS({parent_var}, {q_name}, {cjitem});"
                        ));
                    }

                    lines.extend(annotated_base_build(
                        &cjitem.to_string(),
                        dtype.short_name(),
                        meta,
                    ));

                    lines.push(ln_create_value);
                    lines.push(iformat!(
                        "cJSON_AddItemToObjectCS({cjitem}, \"value\", {cjval});"
                    ));
                    drop(cjitem);
                }
            }
        }
        Field::Const { .. } | Field::Pad(_) => {
            // nothing
        }
        Field::Bits { name, items, meta } => {
            match name {
                Some(_) => {
                    let inner = Field::Struct {
                        name: name.as_ref().unwrap().clone(),
                        fields: items.iter().map(bitfield_to_field).collect(),
                        meta: meta.clone(),
                    };
                    lines.extend(export_field(
                        config,
                        parent_var,
                        parent_is_array,
                        path,
                        cjnamer,
                        tempnamer,
                        &inner,
                        style,
                    ))
                }
                None => {
                    // items extracted to parent level
                    lines.extend(
                        items
                            .iter()
                            .map(bitfield_to_field)
                            .map(|f| {
                                export_field(
                                    config,
                                    parent_var,
                                    parent_is_array,
                                    path,
                                    cjnamer,
                                    tempnamer,
                                    &f,
                                    style,
                                )
                            })
                            .flatten(),
                    );
                }
            }
        }
        Field::Struct { name, fields, meta } => {
            let cjitem = cjnamer.claim();
            lines.push(iformat!("CJSON_TRY({cjitem} = cJSON_CreateObject());"));
            let q_name = name.add_slashes_and_quotes();
            if parent_is_array {
                lines.push(iformat!("cJSON_AddItemToArray({parent_var}, {cjitem});"));
            } else {
                lines.push(iformat!(
                    "cJSON_AddItemToObjectCS({parent_var}, {q_name}, {cjitem});"
                ));
            }

            let subname;
            let cjmembers;

            match style {
                Style::Dataonly => {
                    subname = cjitem.name();
                }
                Style::Annotated => {
                    cjmembers = cjnamer.claim();
                    subname = cjmembers.name();

                    lines.extend(annotated_base_build(&cjitem.to_string(), "struct", meta));

                    lines.push(iformat!("CJSON_TRY({cjmembers} = cJSON_CreateObject());"));
                    lines.push(iformat!(
                        "cJSON_AddItemToObjectCS({cjitem}, \"items\", {cjmembers});"
                    ));
                }
            }

            lines.extend(
                fields
                    .iter()
                    .map(|f| {
                        export_field(
                            config,
                            &subname,
                            false,
                            &path.child(name),
                            cjnamer,
                            tempnamer,
                            f,
                            style,
                        )
                    })
                    .flatten(),
            );
        }
        Field::Array { count, field } => {
            lines.extend(array_build(*count, &count.to_string(), field));
        }
        Field::TailArray { field } => {
            let name = field.get_name().unwrap();
            let count = &path.child(&format!("{}_len", name));
            lines.extend(array_build(
                std::u32::MAX as usize,
                &count.to_string(),
                field,
            ));
        }
    }

    lines
}

/// In comes r#""hello %08x.""# and 32, out comes r#""hello %08" PRIx32 ".""#
fn apply_inttypes_to_quoted_printf_format(fmt : &str, bytesize : usize) -> Cow<'_, str> {
    let bitsize = bytesize * 8;

    lazy_static!{
        static ref RE: Regex = Regex::new(r"(%(?:[-+ 0#])?(?:\d+)?)([diuoxX])").unwrap();
    }
    // the value is only passed once to printf, so we do just one replacement
    return RE.replace(&fmt, |caps: &regex::Captures| {
        format!("{}\" PRI{}{} \"", &caps[1], &caps[2], bitsize)
    });
}

#[cfg(test)]
mod tests {
    use crate::codegen::gen_export_json::apply_inttypes_to_quoted_printf_format;

    #[test]
    fn test_inttypes_subs() {
        let inp = r#""hello %08x.""#;
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 4);
        assert_eq!(outp.as_ref(), r#""hello %08" PRIx32 ".""#);

        let inp = r#""%08X""#;
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 8);
        assert_eq!(outp.as_ref(), r#""%08" PRIX64 """#);

        let inp = r#""hello %d!""#;
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 1);
        assert_eq!(outp.as_ref(), r#""hello %" PRId8 "!""#);
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 2);
        assert_eq!(outp.as_ref(), r#""hello %" PRId16 "!""#);
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 4);
        assert_eq!(outp.as_ref(), r#""hello %" PRId32 "!""#);
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 8);
        assert_eq!(outp.as_ref(), r#""hello %" PRId64 "!""#);

        let inp = r#""hello %i!""#;
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 1);
        assert_eq!(outp.as_ref(), r#""hello %" PRIi8 "!""#);

        let inp = r#""hello % 4u!""#;
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 2);
        assert_eq!(outp.as_ref(), r#""hello % 4" PRIu16 "!""#);

        let inp = r#""hello % 4o!""#;
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 2);
        assert_eq!(outp.as_ref(), r#""hello % 4" PRIo16 "!""#);

        let inp = r#""do not touch this! %% aaa""#;
        let outp = apply_inttypes_to_quoted_printf_format(&inp, 4);
        assert_eq!(outp.as_ref(), r#""do not touch this! %% aaa""#);
    }
}
