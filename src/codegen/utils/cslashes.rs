pub trait AddSlashes {
    /// Add C-style slashes to the string.
    fn add_slashes(&self) -> String;

    /// Add C-style slashes and quotes
    fn add_slashes_and_quotes(&self) -> String {
        let slashed = self.add_slashes();

        let mut result = "\"".to_string();
        let mut x = 0;
        for c in slashed.chars() {
            if x == 1 {
                if matches!(c, 'a'..='f' | 'A'..='F' | '0'..='9') {
                    result.push('"');
                    result.push('"');
                }
                x = 0;
            }

            result.push(c);
            match (x, c) {
                (0, '\\') => {
                    x = 4;
                }
                (4, 'x') => {
                    x = 3;
                }
                (3, _) => {
                    x = 2;
                }
                (2, _) => {
                    x = 1;
                }
                _ => {
                    x = 0;
                }
            }
        }
        result.push('"');

        result
    }
}

impl AddSlashes for String {
    fn add_slashes(&self) -> String {
        self.as_str().add_slashes()
    }
}

impl AddSlashes for &str {
    fn add_slashes(&self) -> String {
        String::from_utf8(
            self.bytes()
                .map(std::ascii::escape_default)
                .flatten()
                .collect::<Vec<u8>>(),
        )
        .expect("utf-8 encoding error")
    }
}

#[test]
fn test_cslashes() {
    assert_eq!("°C".add_slashes_and_quotes(), r#""\xc2\xb0""C""#);
    assert_eq!("°q".add_slashes_and_quotes(), r#""\xc2\xb0q""#);
    assert_eq!("°".add_slashes_and_quotes(), r#""\xc2\xb0""#);
}
