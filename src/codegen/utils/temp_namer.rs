use crate::spec::field::Type;
use std::cell::UnsafeCell;
use std::fmt;

#[derive(Debug)]
struct Loopvar {
    pub depth: usize,
    pub ty: Type,
    pub name: &'static str,
}

impl Loopvar {
    pub const fn new(depth: usize, ty: Type, name: &'static str) -> Self {
        Self { depth, ty, name }
    }
}

#[derive(Debug, Default)]
pub struct TempNamer {
    use8: bool,
    use16: bool,
    use32: bool,
    use64: bool,
    use_str: bool,
    use_int: bool,
    pub use_bitfield_buf: bool, // public to allow clearing it
    loopvari: usize,
    loopvar_types: Vec<Loopvar>,
    descent: usize,
}

impl TempNamer {
    // New tempnamer
    pub fn new() -> Self {
        Default::default()
    }

    pub fn descend<T>(&mut self, inner: impl FnOnce(&mut TempNamer) -> T) -> T {
        self.descent += 1;
        let rv = inner(self);
        self.descent -= 1;
        rv
    }

    /// Get a temporary variable name of the given number of bits
    pub fn tmp(&mut self, bits: usize) -> &'static str {
        match bits {
            1..=8 => {
                self.use8 = true;
                "tmp8"
            }
            9..=16 => {
                self.use16 = true;
                "tmp16"
            }
            17..=32 => {
                self.use32 = true;
                "tmp32"
            }
            33..=64 => {
                self.use64 = true;
                "tmp64"
            }
            _ => {
                panic!("bad tempvar len");
            }
        }
    }

    pub fn tmp_str(&mut self) -> &'static str {
        self.use_str = true;
        "tmpchp"
    }

    pub fn tmp_int(&mut self) -> &'static str {
        self.use_int = true;
        "tmpi"
    }

    pub fn tmp_bytes(&mut self, bytes: usize) -> &'static str {
        self.tmp(bytes * 8)
    }

    pub fn mark_bitfield_buf_used(&mut self) {
        self.use_bitfield_buf = true;
    }

    pub fn bitfield_buf_name(&self) -> &'static str {
        if !self.use_bitfield_buf {
            panic!("Bitfield buf use not declared.");
        }
        "bytebuf"
    }

    /// Get next loop variable, according to the current descent into nested loops.
    ///
    /// Tries to intelligently reuse loop variables.
    ///
    /// # Panics
    /// If more than 26 variables are allocated
    pub fn loopvar(&mut self, maxcount: usize) -> &'static str {
        let ty = Self::get_loop_variable_type_for_max_value(maxcount);

        for entry in self.loopvar_types.iter_mut() {
            if entry.depth < self.descent {
                continue;
            }

            if entry.depth > self.descent {
                entry.depth = self.descent;
            }

            // we can use bigger variable
            if entry.ty.byte_size() < ty.byte_size() {
                // grow the type
                entry.ty = ty;
            }
            return entry.name;
        }

        let name = &"ijklmnopqrstuvwxyzabcdefgh"[self.loopvari..=self.loopvari];
        self.loopvar_types
            .push(Loopvar::new(self.descent, ty, name));
        self.loopvari += 1;
        name
    }

    /// Get type to use for a loop variable given a max value.
    /// This may need to be changed for some platforms (then we will need to get the configuration struct here)
    fn get_loop_variable_type_for_max_value(value: usize) -> Type {
        if value <= 0xFF {
            Type::U8
        } else if value <= 0xFFFF {
            Type::U16
        } else if value <= 0xFFFF_FFFF {
            Type::U32
        } else {
            Type::U64
        }
    }

    pub fn c_declarations(&self) -> String {
        let mut buf = String::new();
        if self.use8 {
            buf.push_str("uint8_t tmp8 = 0;\n")
        }
        if self.use16 {
            buf.push_str("uint16_t tmp16 = 0;\n")
        }
        if self.use32 {
            buf.push_str("uint32_t tmp32 = 0;\n")
        }
        if self.use64 {
            buf.push_str("uint64_t tmp64 = 0;\n")
        }
        if self.use_bitfield_buf {
            buf.push_str("uint8_t bytebuf = 0;\n")
        }
        if self.use_str {
            buf.push_str("char * tmpchp = NULL;\n")
        }
        if self.use_int {
            buf.push_str("int tmpi = 0;\n")
        }

        for entry in self.loopvar_types.iter() {
            buf.push_str(&format!("{} {} = 0;\n", entry.ty.c_name(), entry.name));
        }

        buf
    }
}

/// Temporary variable namer
pub struct NumberedTempNamer {
    // unsafe, we must not use multiple threads here
    vars: UnsafeCell<Vec<bool>>,
    /// Variable prefix
    prefix: &'static str,
}

impl NumberedTempNamer {
    /// Create a new instance with the given prefix
    pub fn new(prefix: &'static str) -> Self {
        Self {
            vars: UnsafeCell::new(vec![]),
            prefix,
        }
    }

    /// Claim a variable for use
    pub fn claim(&self) -> VarGuard {
        for (i, busy) in unsafe { self.vars.get().as_ref() }
            .unwrap()
            .iter()
            .enumerate()
        {
            if !busy {
                unsafe {
                    self.vars.get().as_mut().unwrap()[i] = true;
                }
                return VarGuard::new(self, i);
            }
        }

        let size = unsafe {
            let p = self.vars.get().as_mut().unwrap();
            p.push(true);
            p.len()
        };

        VarGuard::new(self, size - 1)
    }

    /// Release a variable with the given number for re-use
    pub fn release(&self, i: usize) {
        unsafe {
            self.vars.get().as_mut().unwrap()[i] = false;
        }
    }

    /// Get names of all used variables
    pub fn get_names(&self) -> Vec<String> {
        let n = unsafe { self.vars.get().as_ref() }.unwrap().len();
        (0..n).map(|n| format!("{}{}", self.prefix, n)).collect()
    }
}

/// Variable guard, returned from the claim method.
/// Releases the claimed variable for re-use on drop.
pub struct VarGuard<'a> {
    /// Variable number in the tempnamer
    i: usize,
    /// Tempnamer reference
    tempnamer: &'a NumberedTempNamer,
}

impl fmt::Display for VarGuard<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}", self.tempnamer.prefix, self.i)
    }
}

impl<'a> VarGuard<'a> {
    /// Create new instance for a tempnamer reference and variable number
    pub fn new(tempnamer: &'a NumberedTempNamer, i: usize) -> Self {
        Self { i, tempnamer }
    }

    /// Get variable name
    pub fn name(&self) -> String {
        format!("{}{}", self.tempnamer.prefix, self.i)
    }
}

// Release the held variable for re-use on drop
impl<'a> Drop for VarGuard<'a> {
    fn drop(&mut self) {
        self.tempnamer.release(self.i);
    }
}
