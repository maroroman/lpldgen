pub trait ReverseIf {
    fn reverse_if(self, cond: bool) -> Self;
}

impl<T> ReverseIf for Vec<T> {
    fn reverse_if(mut self, cond: bool) -> Self {
        if cond {
            self.reverse();
        }
        self
    }
}
