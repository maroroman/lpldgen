# Payload Specification Syntax

The payload structure is loosely based on C struct syntax with some simplifications
and additions.

## Modules

- The payload system is organized into modules.
- Module can contain any number of payload specification files
- Module can contain any number of sub-modules. Module nesting may be used for logical 
  grouping or to override variables, e.g. to change node addressing.

The root config file references all modules included in code generation.

### Module variables

The root config file, as well as module config files, may contain a `"variables"` field.

Variables may be used as part of the `+to` and `+from` options.

- `"variables"` is a key-value dictionary, mapping strings to `uint64_t` values.
- Variables are assigned before processing a module or descending into its sub-modules.
- Each module creates a copy of the assignment table when descending into a sub-module. 
  This means a sub-module may not affect any parent or sibling modules' variable assignments.
- Variable, once defined, may not be changed. This makes it possible to override a variable
  by defining it in a parent module or the root config file, i.e. to re-address a node.
- Variable names may not start with a digit.

## Payload File Structure

A structure file may contain one or more payload specifications. 
Payloads could be grouped in files e.g. by their source/target device.

## Comments

The following comments are supported:

- `// to the end of line`
- `/* to the matching */` - supports nesting
- `# to the end of line`

Comments are treated as whitespace in all places.

NOTE: Do not use triple slash (`///`) as a general comment,
it has special meaning - doc comment / description. More below in the "Options" section.

## Options

A payload or its fields can be annotated by "options", which are key-value pairs (with pre-defined allowed keys):

```
+name value
+name "value as a quoted string"
+name [value delimited by square brackets]
```

- The basic unquoted syntax can be used for values that do not contain spaces.
- The quoted string syntax supports basic C-style escapes, line `\"`.
- The bracket syntax is useful e.g. for arithmetic expressions, 
  though they can also be given as a string.
- All three formats are equivalent, the quotes, escapes and brackets will be removed.

Options are placed above their payload or a struct field.

### Payload / Field Description

A special case of an option is `+descr` - longer human-readable description
of a payload or field. If a `+descr` option is used multiple times, the values are joined 
by newlines and form a longer text. It is possible to use `\n` in a single `+descr` as well.

This joining behavior is used to allow a more convenient descr syntax: `/// descr here` 
(inspired by Rust's doc comments)

Example:

```
/// This payload has a description
/// that spans multiple lines
+from 12:5
struct MyPayload {
    /// Field with a description
    u8 foo

    // this is just a comment
}
```

## Payload Syntax

A payload definition could look e.g. like this:

```
+from 0x31:8
struct GsNpw_GET_HK_2_VI_Resp {
    u16 vboost[3]
    u16 vbatt
    u16 curin[3]
    u16 cursun
    u16 cursys
    pad 2
}
```

The payload body contains a sequence of fields ordered as they're sent in a byte array 
and subsequently appear in the C struct. Endianness does not reorder payload's fields, 
only the individual field bytes.

### Payload Addressing

Each payload MUST be annotated by a `+from` or `+to` option.

The value is in both cases the same: `CspAddress:CspPort`. The parts are numeric, or a variable name.

This is used by the parsing engine to recognize a payload based on 
its CSP addressing.

```
+to 0x31:8
struct RequestToAddr31Port8 {
    // ...
}

+from 0x31:8
struct ResponseFromAddr31Port8 {
    // ...
}
```

### Excluding payload from recognition

Payloads are, by default, registered in the recognition engine, making it possible to recognize such a payload
by inspecting its binary form and its CSP addressing.

Sometimes a payload has ambiguous addressing, comes from other source than CSP, or collides with other payloads 
and they can only be told apart by other means unknown to the recognition engine. Such payload may be excluded
from the recognition engine. An excluded payload can be still be parsed and built using the dedicated functions, 
which will be generated as usual.

To exclude a payload from recognition, use this syntax:

```
+recognize false
struct PayloadNameHere {
  ...
}
```

Payload names must be unique regardless of whether they are recognized or not, to avoid naming conflicts
between the generated building/parsing functions and payload struct definitions.

## Commas and Semicolons

Commas and semicolons are not mandated, but MAY be placed at the end of each payload field
to approximate C syntax.

## Common Options

These options can be used for the entire payload and/or for individual fields.

### Description

```
+descr "Description..."
```

Payload or field description, human-readable name shown in 
debug formatting and generated Doxygen comments.

Description can be attached to the payload and to each individual field.

It is illegal to specify both `+from` and `+to` for one payload.

### Endianness

Endianness determines the byte order within a field and can be set either for the 
whole payload, or selectively for individual fields.

```
+endian big
+endian little
```

The default endian is big.

Example with 0xABCD

- big ... `MSB,LSB` ... `0xAB, 0xCD`
- little ... `LSB,MSB` ... `0xCD, 0xAB`

Endianness has no effect on fields not longer than one byte.

Endianness can be used inside bitfields and has effect if some of the inner fields are more
than 8 bits long. Such field is then split to byte chunks and those are reordered as needed.

The following examples illustrate how the bitfield packing changes with varied field order, endian, and alignment.

```
bits {
    pad 4
    +endian big
    u16 adc : 12
}
// ____Abcd Efghijkl

bits {
    +endian little
    u16 adc : 12
    pad 4
}
// Efghijkl Abcd____

+align low
bits {
    +endian little
    u16 adc : 12
    pad 4
}
// Efghijkl ____Abcd
```

## Payload Fields

There are several field types, each with a different syntax.

- simple field
- struct
- bitfield
- constant
- padding
- array
- tail array

Their syntax is explained below.

All fields can be annotated by options.

### Unit Annotation

Fields representing physical values can be annotated with their "unit". This will be shown in human-readable 
outputs.

```
+descr "Report from a light sensor"
+from 30:16
struct LightSensorData {
    +unit "W/m²"
    float light_intensity
    +unit °C
    float soc_temperature
}
```

### Field Conversions

Some fields, such as temperature stored in unsigned int, can be
annotated to specify how to convert the value to and from
a float or double. This is usually accompanied by the unit annotation.

Conversion is possible between most combinations of data types, normally
it will be float or double stored as an integer type, but the opposite 
or conversions between different integer types are also possible.

Conversion annotations:

- transform ... conversion formula where `x` is the analog value (°C) and the result
  is the value that goes in the binary payload.
- repr ... data type to use in the source struct

*Type conversion may not be used inside bitfields.*

**Example:**

This field will be stored as u16 in the binary payload, and converted from/to float
when un/packing the struct. It can represent temperatures from -20°C to 6533.5°C with 0.1°C precision.
The field will be called "Battery temperature" in a human-readable report, and annotated by the "°C" unit.

Notice how "Battery temperature" is in quotes - it's needed because of the spaces. The conversion formula
uses square brackets here, but it's a string - quotes would work just as well. If there were no spaces,
it could even be left unquoted.

```
+descr "Battery temperature"
+unit °C
+transform [ round((x + 20) * 10) ]
+repr float
u16 temperature
```

The expression may use basic arithmetic operators that behave like in C:

- `*`
- `+`
- `-`
- `/`
- `round()`, `floor()`, `ceil()`
- parentheses `(`, `)`
- trig: `sin()`, `tan()`, `cos()`, `asin()`, `atan()`, `acos()`

The variable `x` represents the source, analog value. The expression should result
in the binary value to store in the struct. If no rounding function is 
used at the outermost level and the result is integer, `round()` will be added internally.

The provided expression will be algorithmically reversed to produce the
opposite conversion - e.g. integer to float.

### String formats in JSON output

Sometimes, a value is better represented as hex or with a fixed number of decimal places.
This can be done using the `+fmt` annotation. The value then becomes a string using the specified
`sprintf` format when exported to JSON.

```
+fmt 0x%04x
u16 crc
```

The format string can optionally be quoted and supports escape sequences.

lpldgen internally transforms printf specifiers to platform-independent form using macros from
`<inttypes.h>`, such as `"0x%04" PRIx16`, with the bit size based on the payload field it's used on.

### Primitive Types

The following names can be used to describe data types in a payload:

- u8
- u16
- u24
- u32
- u64
- char, i8
- i16
- i24
- i32
- i64
- f32, float
- f64, double
- bool

`<stdint.h>` types (e.g. `uint8_t`) can also be used.

### Simple Field

Basic field is defined by its options (if any), type and name.

```
+endian little
+descr "Thrust"
u64 thrust
```

### Struct

Struct defines a group of fields, either for logical grouping,
or for repetition as an array of structs.

```
struct StructName {
    // Inner fields here
    u8 status
    u16 errors
}
```

If a struct is annotated with `+endian`, that endianness will be the default 
for all its fields.

### Fixed Length Array

Fixed length array has the same syntax as a simple field,
except the name is suffixed by its size in square brackets:

```
u64 thrust[4] # 4 engines 

// Array of ten structs
struct LEDs[10] {
    bool enabled
    u8 pwm
}
```

### Tail Array

Tail array is an array of arbitrary length at the end of the struct.

```
u8 raw_bytes[...]
```

The real tail array's length is stored in a `payload.<name>_len` field of the data struct
when parsed, or when given to the payload engine for packing to bytes.

A struct containing such array must be dynamically allocated. The generated header file includes 
a helper macro for each such struct to get the required allocation size for a particular length.

```c
/** Payload_tail_bits tail size */
#define Payload_tail_bits_TAIL_SIZE (sizeof(((struct Payload_tail_bits*)0)->tail[0]))

/** Payload_tail_bits sizeof helper */
#define Payload_tail_bits_SIZE(_tail_count) (sizeof(struct Payload_tail_bits) + Payload_tail_bits_TAIL_SIZE*(_tail_count))
```

After allocating the struct, make sure the `<tail_field>_len` member is set to the tail element count.

### Padding Bytes

Padding inserts don't-care bytes into the payload. Their value is unspecified 
(though in built payloads they are always zeroed out) and they are skipped when 
parsing the payload.

There are two ways to define a padding:

- `pad <bytes>` - e.g. `pad 6`
- `u32 _` - a field named `_` becomes padding of the data type's byte size

### Constants

Payloads can contain fixed, constant bytes. These fixed bytes are used by the parser
to disambiguate between payloads with the same CSP addressing and length. Constant bytes 
do not appear in the data struct, only in the binary payload.

The syntax is identical to simple fields, but they use an equals
sign followed by the value instead of a name (`=123`). 

Constant values can be either decimal, hexadecimal with the 0x prefix, or binary prefixed with 0b.
Underscores may be freely used to group digits and will be discarded by the parser.

```
u8 =0b1111_0101
u16 =0xFFC0
i32 =-123456
```

Zero constant differs from padding in that it must be zero for the payload to be recognized;
padding bytes can have any value.

Constants, like most other fields, can be annotated to specify their endianness. Other annotations
are allowed as well, but generally won't have any visible effect.

### Bitfields

A bitfield is a set of fields that do not align with the byte boundary.

**The bitfield itself must be byte-aligned and have even length (multiple of 8 bits)**

Bitfield is a form of struct, therefore the syntax is similar. 
Each field inside a bitfield must be suffixed by its bit size preceded by a colon.

```
// named bitfield stays as a struct
bits name {
    u8 three_bits : 3

    +endian little
    u16 ten_bits : 10

    u8 =0xFC : 8

    pad 2

    _ : 1
}

// unnamed bitfield is expanded to the inner fields in the data struct
bits {
    // ...
}
```

#### Bitfield Alignment

Bitfield introduces another complexity on top of endianness: field alignment.

Fields inside a bitfield can be aligned left (to MSb) or right (to LSb). This determines in which direction
are the bits of a byte used up. The produced bytes are NOT reordered by endian.

Default alignment is `left`, or `high`, placing the bitfield members to the highest bits of a byte first.

Endian in a bitfield affects only multi-byte fields, such as `u16 x : 12` - 
then the field is split to `11..8` and `7..0` and those bit chunks are ordered
according to the endian. Default is big endian (endianness is inherited from the parent struct or bitfield).

*The effect of alignment is illustrated in the following examples:*

```
+align left
bits {
    u8 a : 3
    u8 b : 8
    u8 c : 5
    u8 d : 7
    pad 1
}
// byte 0: AaaBbbbb
// byte 1: bbbCcccc
// byte 3: Ddddddd_

+align right
bits {
    u8 a : 3
    u8 b : 8
    u8 c : 5
    u8 d : 7
    pad 1
}
// byte 0: bbbbbAaa
// byte 1: CccccBbb
// byte 3: _Ddddddd
```

#### Bitfield Padding

Padding in a bitfield takes the forms shown in the following example:
 
 ```
bits {
    // these two forms are equivalent
    pad <bits>
    _ : <bits>
}
 ```

Any field named `_` also becomes padding.

#### Bitfield Constants

Constants in a bitfield work the same way as regular constant fields. They are simply suffixed by the bit size:

```
bits {
    u8 =0x3F : 6
    pad 2
}
```

#### Bitfield Structs and Arrays 

Bitfields may contain structs and arrays.

Structs and struct arrays in a bitfield may be freely nested, and do not need to be byte aligned or evenly sized.

```
bits {
    /* the struct has length 10 bits */
    struct Nested {
        u8 a : 5
        u8 b : 5
    }
    /* pad to whole bytes */
    pad 6
}
```

```
bits BitfieldWithArray {
    /* array of five 3-bit fields */
    u8 foo[5] : 3

    /* array of two 10-bit structs */
    struct Nested[2] {
        u8 a : 5
        u8 b : 5
    }
    /* 5-bit constant */
    u8 =0b11011 : 5
}
```

#### Repeated bitfield

This shorthand syntax

```
bits name[8] {
    u8 item : 3
}
```

means the same as

```
bits {
    struct name[8] {
        u8 item : 3
    }
}
```

This bitfield struct array must have even byte size (here 3*8 = 24)

#### Bitfield tail array

This shorthand syntax

```
bits name[...] {
    u8 item : 3
    u8 item2 : 5
}
```

means the same as

```
struct name[...] {
    bits {
        u8 item : 3
        u8 item2 : 5
    }
}
```

Each element of such bitfield tail array must have even byte size.

---

## Payload templates

Parts of a payload can be re-used using templates.

A template, when used, is replaced by its fields (without a wrapping struct).
A struct may be placed inside the template, or around the use statement, as needed.

It is also possible to use a template inside an array of struct, simply use it in the struct's body.

```
template Header {
    u8 foo;
    u16 bar;
}

template Word {
    u32 w;
}

+from 30:16
struct SomePayload {
    use Header;
    
    struct s {
        use Word;
    }
    
    f32 value;
}
```

the above will become:

```
+from 30:16
struct SomePayload {
    u8 foo;
    u16 bar;
    struct s {
        u32 w;
    }
    f32 value;
}
```

Templates may be used in any nested struct as well.

It's possible to change endianity of an applied template using the `+endian` option:

```
template TwoWords {
    u32 word1;
    u32 word2;
    
    // if endianity is specified per field,
    // it won't be changed.
    +endian big
    u32 always_big_endian;
}

+from 30:16
struct SomePayload {
    +endian big
    use TwoWords;
    +endian little
    use TwoWords;
}
```

### Templates from different module

To use a template from a different module (in the same project), prefix it with the module's name and slash, e.g.

```
struct Foo {
    use MyTemplates/Header;
}
```

If a module contains nothing but templates, it will not generate any .c/.h files on its own.
